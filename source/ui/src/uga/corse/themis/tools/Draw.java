package uga.corse.themis.tools;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Map.Entry;

import uga.corse.themis.algorithms.choreography.Choreography;
import uga.corse.themis.algorithms.choreography.MonChor;
import uga.corse.themis.algorithms.choreography.MonitorNetwork;
import uga.corse.themis.algorithms.choreography.Score;
import uga.corse.themis.algorithms.choreography.MonitorNetwork.NetNode;
import uga.corse.themis.automata.Automata;
import uga.corse.themis.expressions.ltl.LTLExpression;
import uga.corse.themis.monitoring.specifications.Specification;
import uga.corse.themis.utils.*;
import uga.corse.themis.utils.mapper.DefaultSymbolMapper;
import uga.corse.themis.monitoring.specifications.SpecLTL;
import uga.corse.themis.utils.Opts.OptError;

/**
 * Command-Line tool to run the formulas
 * @author Antoine El-Hokayem <antoine.el-hokayem@imag.fr>
 */
public class Draw  {
	private static Opts options = initOptions();
	private static Opts initOptions() {
		Opts opts = new Opts();
		opts.addOpts(
			"-ltl", true , 1, "Formula",
			"-ltlf",true , 1, "File with formula on the first line",
			"-spec",true , 1, "Specification File (XML)",
			"-simp",true , 0, "Apply LTL simplification to input formula",
			"-net" ,true , 0, "Generate the network",
            "-expr",true , 0, "Generate simplified delta expressions",
            "-only",true , 1, "Draw only specs with provided keys (comma separated)"
		);
		return opts;
	}
	public static class Options {
		public boolean fromSpec = false;
		public Map<String, Specification> ltl = new HashMap<>();
		public boolean simplify = false;
		public boolean net      = false;
		public boolean simpExpressions = false;
		public String[] only = null;
	}
	public static void npos(int n) throws OptError {
		if(n <= 0) throw new OptError(n + " must be > 0");
	}
	public static void nzer(int n) throws OptError {
		if(n <  0) throw new OptError(n + " must be >= 0");
	}

	public static void main(String[] args) throws Exception {
		Options config = new Options();
		options.registerActions(
			"-simp",(OptAction) x -> config.simplify = true,
			"-ltl", (OptAction) x -> {
				;
				SpecLTL spec = new SpecLTL(x[0]);
				if(spec.getLTL() == null)
					throw new OptError("Could not parse LTL: " + x[0]);
				config.ltl.put(x[0], spec);
				
			},
			"-ltlf", (OptAction) x -> {
				File ltl = new File(x[0]);
				if(!ltl.exists())
					throw new OptError(x[0] + ": does not exist");
				Scanner scan = new Scanner(ltl);
				String ltlf = scan.nextLine();
				config.ltl.put(ltlf,  new SpecLTL(ltlf));
				scan.close();
			},
			"-spec", (OptAction) x -> {
				File ltl = new File(x[0]);
				if(!ltl.exists())
					throw new OptError(x[0] + ": does not exist");
				config.fromSpec = true;
				config.ltl =  SpecLoader.loadSpec(ltl);
			},
            "-only", (OptAction) x -> {
			    if(x[0].length() == 0)
			        throw new OptError("Please provide a comma separated list of keys to ignore");
                config.only = x[0].split(",");
            },
			"-net", (OptAction) x -> {config.net = true;},
            "-expr", (OptAction) x -> {config.simpExpressions = true;}
		);
		
		args = Opts.process(args, options);
		
		if(config.ltl == null) {
			System.err.println("No formula given");
			options.printUsage();
		}
		Map<String, Specification> toIterate;
		if(config.only != null){
		    toIterate = new HashMap<>();
		    for(String k : config.only) {
		        Specification spec = config.ltl.get(k);
		        if(spec == null)
		            System.err.println("Warning: no specification found with key " + k);
		        else
		            toIterate.put(k, spec);
            }
        }
        else
            toIterate = config.ltl;

		for(Entry<String, Specification> entry : toIterate.entrySet()) {
			if(entry.getValue() instanceof SpecLTL) {
				SpecLTL ltl = (SpecLTL) entry.getValue();

				if (config.simplify) {
					Simplifier simp = SimplifierFactory.getDefault();
					String ltls = ltl.toString();
					String ltlsimp = simp.simplify(ltls);
					ltl = new SpecLTL(ltlsimp);
					System.out.println("[LTL Simplified] " + ltl.toString());
				}

				if (!config.net) {
					Automata aut = Convert.makeAutomataSpec(ltl).getAut();
					if (config.simpExpressions) {
						aut = Convert.simplifyTransitions(aut);
					}
					UI.draw(aut, entry.getKey());
				} else {
					net(ltl, config.simpExpressions);
				}
			}
			else {
				Automata aut = Convert.makeAutomataSpec(entry.getValue()).getAut();
				if(aut == null) {
					System.err.println("Specification: " + entry.getKey() + " cannot be converted to automata");
					UI.draw(aut, entry.getKey());
				}
			}
		}
	}

	
	private static void net(SpecLTL ltlspec, boolean simplify) {
	    LTLExpression spec = ltlspec.getLTL();

	    Score data = Score.getScoreTree(spec, new DefaultSymbolMapper());
	    
	    Choreography.resetId(0);
	    //Split the LTL into different monitors
	    MonitorNetwork net = Choreography.splitFormula(Choreography.newId(), data.comp, data, spec, false);
	    Choreography.compactNetwork(net);
	    //Compute Respawn for each monitor
	    Set<Integer> resp  = Choreography.respawn(net, net.root.spec, false);
	    
	    Map<Integer, MonChor> mons = new HashMap<Integer, MonChor>(net.nodes.size());
	    Map<Integer, String> forms = new HashMap<>();
	    
	    //Initialize and Attach Monitors
	    for(NetNode node : net.nodes.values()) {
	      MonChor mon = new MonChor(node.id, Convert.makeAutomataSpec(new SpecLTL(node.spec)).getAut());
	      mon.setRespawn(resp.contains(node.id));
	      mons.put(mon.getID(), mon);
	      forms.put(mon.getID(), node.spec.toString());
	    }
	    
	    //Update: Ref/CoRef
	    for(Entry<Integer, Set<Integer>> entry : net.edges.entrySet()){
	      Integer from = entry.getKey();
	      for(Integer to : entry.getValue()) {
	        mons.get(from).getRef().add(to);
	        mons.get(to).getCoref().add(from);
	      }
	    }
	    for(MonChor mon : mons.values()) {
			Automata aut = mon.getAutomata();
			if(simplify)
				aut = Convert.simplifyTransitions(aut);
			UI.draw(aut, "m_" + mon.getID() + ": " + forms.get(mon.getID()));
	    }
	   
	}
}
