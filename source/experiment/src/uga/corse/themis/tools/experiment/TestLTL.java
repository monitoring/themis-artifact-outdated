/**
 * TestLTL
 * <p>
 * name <email>
 */
package uga.corse.themis.tools.experiment;

import uga.corse.themis.monitoring.specifications.SpecLTL;
import uga.corse.themis.monitoring.specifications.Specification;

import java.util.Map;


public class TestLTL implements TestOracle {
    @Override
    public boolean verify(Map<String, Specification> spec) {
        return Verify.verify((SpecLTL) spec.get("root"));
    }
}
