package uga.corse.themis.tools.experiment;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uga.corse.themis.Platform;
import uga.corse.themis.Topology;
import uga.corse.themis.comm.dispatcher.Dispatcher;
import uga.corse.themis.comm.messages.MsgCreateComponent;
import uga.corse.themis.comm.messages.MsgPlatformData;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.runtime.Deploy;
import uga.corse.themis.utils.Opts;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.Semaphore;

public class RunTraces implements Runnable {
    private static Logger log = LogManager.getLogger(Experiment.class);
    private static final String DEFAULT_PREIPHERY = "uga.corse.themis.periphery.TraceFile";

    private Integer start;
    private Integer end;
    private List<MsgCreateComponent> comps;
    private Platform platform;
    private Set<String> allNodes;
    private String tracesDir;
    private String notify;
    private Semaphore lock;
    private Map<String, Serializable> tags;
    private Dispatcher disp;
    private Set<? extends Monitor> mons;
    private String target;

    public RunTraces(Integer start, Integer end, List<MsgCreateComponent> comps, Set<? extends Monitor> mons,
                     Platform platform, Set<String> allNodes, String target, String tracesDir, String notify, Semaphore lock,
                     Map<String, Serializable> tags, Dispatcher disp) {
        this.start = start;
        this.end = end;
        this.comps = comps;
        this.mons = mons;
        this.platform = new Platform();
        this.platform.setData(platform.getData());
        this.allNodes =  allNodes;
        this.tracesDir = tracesDir;
        this.notify = notify;
        this.lock = lock;
        this.disp = disp;
        this.tags = new HashMap<>();
        this.tags.putAll(tags);
        this.target = target;
    }

    @Override
    public void run() {

        Topology top = new Topology();
        int nrcomps = comps.size();
        String traceClass = Opts.getGlobal("THEMIS_EXP_PERIPHERY", DEFAULT_PREIPHERY);

        try {
            for (int trace = start; trace <= end; trace++) {
                tags.put("tid", trace + "");
                platform.getMonitors().clear();

                Iterator<MsgCreateComponent> iter = comps.iterator();
                //Deploy Components
                for (int k = 0; k < nrcomps; k++) {
                    String name = ((char) ('a' + k)) + "";
                    top.add(name);
                    MsgCreateComponent template = iter.next();
                    MsgCreateComponent info     = new MsgCreateComponent(
                            template.getId(), target, template.getCls()
                    );
                    String path = tracesDir + trace + "-" + name + ".trace";
                    info.addInput(traceClass, path);

                    if (!Deploy.deploy(disp, platform, info))
                        Deploy.fail(disp, allNodes);
                }


                //Deploy Monitors
                for (Monitor mon : mons)
                    if (!Deploy.deploy(disp, platform, mon))
                        Deploy.fail(disp, allNodes);

                //Send Platform Data
                if (!disp.cmdSync(target, new MsgPlatformData(platform)))
                        Deploy.fail(disp, allNodes);


                Set<String> all = new HashSet<>();
                all.add(target);
                //Start and Wait
                if (!Deploy.signalStart(disp, all, 0l, notify, tags))
                    Deploy.fail(disp, allNodes);

                lock.acquire();
            }
        }
        catch(Exception e) {
            log.fatal(e.getLocalizedMessage(), e);
            System.exit(1);
        }


    }
}
