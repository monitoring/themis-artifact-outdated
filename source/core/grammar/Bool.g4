grammar Bool;

@header{ package uga.corse.themis.parser.backend; }

expr
	: '(' expr ')'	 #BOOL_PAREN
	| '!' expr		 #BOOl_NOT
	| expr '&' expr  #BOOL_AND
	| expr '|' expr	 #BOOL_OR
	| STRING		 #BOOL_SYM
	| '1'			 #BOOL_TRUE
	| '0'			 #BOOl_FALSE
	;

STRING 
	: LETTER (LETTER | DIGIT | '_')*
	;

fragment LETTER
   : [a-z]
   ;

fragment DIGIT
   : [0-9]
   ;

WS
   : [ \t\n\r]+ -> skip
   ;
