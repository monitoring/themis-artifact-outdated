grammar Labels;

@header{ package uga.corse.themis.parser.backend; }

label
	: '(<empty>)'	      #L_EMPTY
	| event			        #L_event
	;
event
	: event '&&' expr   #E_COMPOSITE
	|'(' event ')'	    #EV_PAR
	| expr			        #E_SINGLE
	;
expr
	: '(' expr ')'	    #E_PAREN
	| atom			        #E_ATOM
	;

atom
	: obligation
	| STRING
	;


obligation
	: '<' INT ',' STRING '>'
	; 

STRING 
	: LETTER (LETTER | DIGIT)*
	;
INT
	: DIGIT+
	;

fragment LETTER
   : [a-zA-Z\u0080-\u00FF_]
   ;

fragment DIGIT
   : [0-9]
   ;


WS
   : [ \t\n\r]+ -> skip
   ;
