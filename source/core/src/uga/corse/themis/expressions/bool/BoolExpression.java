package uga.corse.themis.expressions.bool;

import uga.corse.themis.automata.Event;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.expressions.Expression;
import uga.corse.themis.monitoring.memory.Memory;

import java.io.Serializable;
import java.util.List;

public interface BoolExpression extends Expression, Serializable {
    Boolean eval(Event e);

    @SuppressWarnings("rawtypes")
    Verdict eval(Memory m);

    List<Object> getOperands();
}
