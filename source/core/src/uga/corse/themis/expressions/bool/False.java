package uga.corse.themis.expressions.bool;

import java.util.List;

import uga.corse.themis.automata.Event;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.monitoring.memory.Memory;

@SuppressWarnings("serial")
public class False implements BoolSym {
    @Override
    public Boolean eval(Event e) {
        return false;
    }

    @Override
    public String toString() {
        return "false";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Verdict eval(Memory m) {
        return Verdict.FALSE;
    }

    @Override
    public List<Object> getOperands() {
        return null;
    }


}
