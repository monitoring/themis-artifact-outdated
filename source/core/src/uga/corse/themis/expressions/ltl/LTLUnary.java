package uga.corse.themis.expressions.ltl;

public class LTLUnary extends LTLExpressionBase implements LTLExpressionUnary {

    public LTLUnary(LTLOperator op, LTLExpression expr) {
        super(op, (LTLExpression[]) null);
        exprs = new LTLExpression[1];
        exprs[0] = expr;
    }

    @Override
    public LTLExpression getOperand() {
        return exprs[0];
    }

    @Override
    public String toString() {
        return op.toString() + "(" + exprs[0].toString() + ")";
    }
}
