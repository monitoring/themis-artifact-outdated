package uga.corse.themis.expressions.ltl;

import uga.corse.themis.expressions.ExpressionUnary;

public interface LTLExpressionUnary extends LTLExpression, ExpressionUnary<LTLExpression> {
    LTLExpression getOperand();
}
