package uga.corse.themis.bootstrap;


import uga.corse.themis.monitoring.specifications.Specification;

import java.util.Map;

public interface StandardMonitoring extends Bootstrap {
    void setSpecification(Map<String, Specification> specs);
}
