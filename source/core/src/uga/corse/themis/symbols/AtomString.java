package uga.corse.themis.symbols;

import uga.corse.themis.automata.Atom;

/**
 * A basic atom symbol as string
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class AtomString implements Atom {
    private static final long serialVersionUID = 1L;
    private String sym;


    public AtomString(String ap) {
        sym = ap;
    }

    @Override
    public String observe() {
        return sym;
    }

    @Override
    public String toString() {
        return sym;
    }

    @Override
    public Object group() {
        return null;
    }

    @Override
    public boolean equals(Object atom) {
        if (atom instanceof AtomString)
            return ((AtomString) atom).sym.equals(sym);
        else
            return false;
    }

    @Override
    public int hashCode() {
        return sym.hashCode();
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public int compareTo(Atom arg0) {
        if (arg0 == null) return -1;
        if (arg0 instanceof AtomString)
            return this.sym.compareTo(((AtomString) arg0).sym);

        return this.observe().compareTo(arg0.observe());
    }

}
