package uga.corse.themis.symbols;

import uga.corse.themis.automata.Atom;

/**
 * A negation atom, that negates another atom
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class AtomNegation implements Atom {
    private static final long serialVersionUID = 1L;
    private static String SYM_NEGATE = "!";
    private Atom base;

    public AtomNegation(Atom sym) {
        this.base = sym;
    }

    @Override
    public String observe() {
        return SYM_NEGATE + base.observe();
    }

    @Override
    public String toString() {
        return SYM_NEGATE + base.toString();
    }

    @Override
    public Object group() {
        return base.group();
    }

    @Override
    public boolean equals(Object atom) {
        if (atom instanceof AtomNegation)
            return ((AtomNegation) atom).base.equals(base);
        else
            return false;
    }

    @Override
    public int hashCode() {
        return observe().hashCode();
    }

    @Override
    public boolean isEmpty() {
        return base.isEmpty();
    }

    /**
     * Returns the negated atom
     *
     * @return
     */
    public Atom getNegated() {
        return base;
    }

    @Override
    public int compareTo(Atom arg0) {
        if (arg0 == null) return -1;
        if (arg0 instanceof AtomNegation)
            return this.base.compareTo(((AtomNegation) arg0).base);

        return this.base.observe().compareTo(arg0.observe());
    }

}
