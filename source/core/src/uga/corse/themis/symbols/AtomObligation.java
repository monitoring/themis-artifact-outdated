package uga.corse.themis.symbols;

import uga.corse.themis.automata.Atom;

/**
 * Adds a timestamp obligation for a given atom
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class AtomObligation implements Atom {
    private static final long serialVersionUID = 1L;
    private Atom atom;
    private int timestamp;

    public AtomObligation(Atom ap, int t) {
        atom = ap;
        timestamp = t;
    }

    @Override
    public String toString() {
        return "<" + timestamp + ", " + atom.observe() + ">";
    }

    @Override
    public String observe() {
        return timestamp + "-" + atom.observe();
    }

    @Override
    public int compareTo(Atom o) {
        if (o instanceof AtomObligation)
            return this.timestamp - ((AtomObligation) o).timestamp;

        return -1;
    }

    @Override
    public Object group() {
        return timestamp;
    }

    @Override
    public boolean equals(Object atom) {
        if (atom instanceof AtomObligation) {
            AtomObligation a = (AtomObligation) atom;
            return this.atom.equals(a.atom) && a.timestamp == timestamp;
        } else
            return false;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public Atom getAtom() {
        return atom;
    }

    @Override
    public int hashCode() {
        return observe().hashCode();
    }

    @Override
    public boolean isEmpty() {
        return atom.isEmpty();
    }
}