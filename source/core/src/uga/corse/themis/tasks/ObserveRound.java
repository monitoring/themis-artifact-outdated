package uga.corse.themis.tasks;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uga.corse.themis.automata.Atom;
import uga.corse.themis.monitoring.Component;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.monitoring.MonitorResult;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.monitoring.memory.MemoryAtoms;


import uga.corse.themis.encoders.EncTimestamp;
import uga.corse.themis.encoders.Encoder;
import uga.corse.themis.monitoring.memory.MemoryRO;
import uga.corse.themis.runtime.Node;
import uga.corse.themis.runtime.Runtime;
import uga.corse.themis.symbols.AtomNegation;

import java.util.*;
import java.util.concurrent.*;

public class ObserveRound implements Callable<MonitorResult> {

    private static Logger log = LogManager.getLogger(ObserveRound.class);


    private Component comp;
    private int round;
    private Collection<Monitor> monitors;
    private final Node node;


    public ObserveRound(final Node node, int round, Component comp, Collection<Monitor> mons) {
        this.round      = round;
        this.comp       = comp;
        this.monitors   = mons;
        this.node       = node;
    }

    @Override
    public MonitorResult call() throws Exception {
        Memory<Atom> obs    = comp.observe();

        Memory<Atom> result = new MemoryAtoms();
        Encoder encoder     = new EncTimestamp(round);
        for (Atom atom : obs.getData()) {
            Atom enc = encoder.encode(atom);
            if (enc instanceof AtomNegation)
                result.add(((AtomNegation) enc).getNegated(), false);
            else
                result.add(enc, true);
        }

        final MemoryRO<Atom> observations = new MemoryRO<>(result);

        if (monitors.isEmpty()) return MonitorResult.CONTINUE;


        final ExecutorService pool = Runtime.getScheduler();
        CompletionService<MonitorResult> scheduler = new ExecutorCompletionService<>(pool);

        List<Future<MonitorResult>> tasks = new ArrayList<>(monitors.size());
        Map<Integer, Monitor> monMap     = new HashMap<>(monitors.size());
        for (Monitor mon : monitors) {
            Future<MonitorResult> task =  scheduler.submit(new MonitorRound(mon, observations, round));
            monMap.put(task.hashCode(), mon);
            tasks.add(task);
        }

        //Just used a size to wait on
        for(Future<MonitorResult> task : tasks)
        {
            Future<MonitorResult> future = scheduler.take();
            MonitorResult res = future.get();
            Monitor mon = monMap.get(future.hashCode());
            log.trace("[" + mon.getID() + "] (" + mon.getCurrentVerdict() + ") " + res);
            //Handle Aborts
            if (res.isAbort()) {
                node.notifyAbort(mon.getID());
                // Cancel all tasks
                for (Future<MonitorResult> task2 : tasks)
                    task2.cancel(true);
                return res;
            }
            //Notify NodeRounds in case of verdict
            else if (res == MonitorResult.VERDICT) {
                node.notifyVerdict(mon.getID(), mon.getCurrentVerdict());
            }
        }
        return MonitorResult.CONTINUE;
    }

}
