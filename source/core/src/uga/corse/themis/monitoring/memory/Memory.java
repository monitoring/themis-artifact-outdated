package uga.corse.themis.monitoring.memory;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.automata.Event;
import uga.corse.themis.automata.Verdict;

import java.io.Serializable;
import java.util.Collection;

/**
 * The Memory interface for storing information
 *
 * @param <K> Datatype contained in the memory (Encoding)
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public interface Memory<K> extends Cloneable, Serializable {

    /**
     * Add an observation to memory
     * Observations are grouped into events using the object retuned by
     * {@link Atom#group()}
     *
     * @param a Atomic observation to add
     * @param observed Has the atom been observed to be true or false?
     */
    void add(Atom a, boolean observed);

    /**
     * Has {a} been observed?
     *
     * @param a Atom
     * @return true if it has been observed
     */
    Verdict get(Atom a);

    /**
     * Is the event stored in memory?
     * Returns Verdict.NA if event not in memory
     *
     * @param e Event to check
     * @return Verdict.NA if event not in memory, otherwise its corresponding observation
     */
    Verdict get(Event e);


    /**
     * Memory contains no records
     *
     * @return true if memory contains nothing
     */
    boolean isEmpty();

    /**
     * Merge {m} with this memory
     *
     * @param m memory to merge with current memory
     */
    void merge(Memory<K> m);

    /**
     * Empty Memory
     */
    void clear();

    /**
     * Remove specific K from Memory
     *
     * @param keys Elements to remove from memory
     */
    void remove(Collection<K> keys);

    /**
     * Return memory data
     *
     * @return Elements stored in the memory
     */
    Collection<K> getData();

    Object clone() throws CloneNotSupportedException;

    /**
     * Groups memory elements by a key
     * An atom may specify null to group by its string content
     * Otherwise it can specify a key such as timestamp or component id etc.
     * Memory events are formed using key:
     * atoms with the same key belong to the same event
     *
     * @param a Atom to group
     * @return Object that represents the key
     */
    static Object getKey(Atom a) {
        Object key = a.group();
        if (key == null)
            return a.observe();
        else
            return key;
    }

    /**
     * Get the Key of an event (a group of atoms)
     * See: {@link #getKey(Atom)}
     * @param e Event
     * @return Object key
     */
    static Object getKey(Event e) {
        if (e.empty()) return null;
        return getKey(e.getAtoms().iterator().next());
    }
}
