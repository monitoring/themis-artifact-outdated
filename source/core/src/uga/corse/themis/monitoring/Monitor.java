package uga.corse.themis.monitoring;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.comm.protocol.Message;
import uga.corse.themis.comm.protocol.Network;
import uga.corse.themis.monitoring.memory.MemoryRO;

import java.io.Serializable;

/**
 * A monitor that needs to be setup by the algorithm
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public interface Monitor extends Serializable {
    /**
     * Setup the monitor, this is run only once before reading all traces
     */
    void setup();

    void cleanup();


    MonitorResult monitor(MemoryRO<Atom> observations)
            throws Exception;


    /**
     * Called after all monitors have done their monitor function
     * Typically used for specific communication
     */
    void communicate();


    /**
     * Setup the monitor, this is run for every trace before its read
     */
    void reset() throws Exception;


    /**
     * Return the monitor ID
     *
     * @return
     */
    int getID();

    /**
     * Send a message to another monitor
     *
     * @param to
     * @param msg
     */
    void send(int to, Message msg);

    /**
     * Read the received message
     *
     * @param consume peek (false) or consume (true) the message read
     * @return
     */
    Message recv(boolean consume);

    /**
     * Return the current verdict of the monitor
     * @return
     */
    Verdict getCurrentVerdict();

    void setNetwork(Network net);
    Network getNetwork();

    void setComponentKey(String  attachedComponent);
    String getComponentKey();

}
