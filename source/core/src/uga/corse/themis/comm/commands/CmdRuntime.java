package uga.corse.themis.comm.commands;


import uga.corse.themis.comm.messages.MsgRuntime;
import uga.corse.themis.runtime.Node;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.channels.AsynchronousSocketChannel;

public interface  CmdRuntime<T extends MsgRuntime> {
    boolean handle(T msg, Node n, AsynchronousSocketChannel
            client, ObjectInputStream in, ObjectOutputStream out)
            throws Exception, Error;
}
