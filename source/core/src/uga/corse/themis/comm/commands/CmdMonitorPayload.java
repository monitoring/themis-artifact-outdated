package uga.corse.themis.comm.commands;


import uga.corse.themis.runtime.Node;
import uga.corse.themis.comm.messages.MsgMonitorPayload;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.channels.AsynchronousSocketChannel;

/**
 * Command to add a Component to the Runtime
 */
public class CmdMonitorPayload implements CmdRuntime<MsgMonitorPayload> {
    @Override
    public boolean handle(MsgMonitorPayload msg, Node n, AsynchronousSocketChannel client,
                          ObjectInputStream in, ObjectOutputStream out)
    throws Error, Exception {

        synchronized (n) {
            if(n.isStarted())
                n.getNetwork().put(msg.getTo(), msg.getMessage());
            else
                return false;
        }
        return true;

    }
}
