package uga.corse.themis.comm.commands;


import uga.corse.themis.runtime.Node;
import uga.corse.themis.runtime.Runtime;
import uga.corse.themis.comm.messages.MsgAbort;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.channels.AsynchronousSocketChannel;

/**
 * Command to Abort Monitoring, Reset state
 */
public class CmdAbort implements CmdRuntime<MsgAbort> {
    @Override
    public boolean handle(MsgAbort msg, Node n, AsynchronousSocketChannel client,
                          ObjectInputStream in, ObjectOutputStream out)
    throws Error, Exception {
        Runtime.report("Received Abort: Resetting");
        n.reset();
        return true;
    }
}
