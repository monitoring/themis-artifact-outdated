package uga.corse.themis.comm.commands;


import uga.corse.themis.runtime.Node;
import uga.corse.themis.comm.messages.MsgStart;
import uga.corse.themis.runtime.NodeObserver;
import uga.corse.themis.runtime.observers.DispatcherEnd;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Command to add a Component to the Runtime
 */
public class CmdStart implements CmdRuntime<MsgStart> {
    @Override
    public boolean handle(MsgStart msg, Node n, AsynchronousSocketChannel client,
                          ObjectInputStream in, ObjectOutputStream out)
    throws Error, Exception {

        final Date start = msg.getStart();
        //No Date set
        if(start == null)
            return false;
        //Date is in the past?
        if(Calendar.getInstance().after(start))
            return false;
        //We have already started
        if(n.isStarted()) return false;

        Timer timer = new Timer();

        if(msg.getNotify() != null && !msg.getNotify().isEmpty()) {
            DispatcherEnd obs = new DispatcherEnd(n.getDispatcher(), msg.getNotify());
            n.observerRegister(obs);
        }

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(!n.isStarted())
                    n.start(msg.getTags());
            }
        }, start);

        return true;
    }
}
