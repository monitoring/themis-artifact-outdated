package uga.corse.themis.comm.commands;


import uga.corse.themis.runtime.Node;
import uga.corse.themis.comm.messages.MsgPlatformData;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.channels.AsynchronousSocketChannel;

/**
 * Command to add a Component to the Runtime
 */
public class CmdPlatformData implements CmdRuntime<MsgPlatformData> {
    @Override
    public boolean handle(MsgPlatformData msg, Node n, AsynchronousSocketChannel client,
                          ObjectInputStream in, ObjectOutputStream out)
    throws Error, Exception {
        return n.changePlatform(msg.getPlatform());

    }
}
