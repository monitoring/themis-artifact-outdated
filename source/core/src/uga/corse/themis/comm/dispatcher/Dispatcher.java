package uga.corse.themis.comm.dispatcher;

import uga.corse.themis.comm.messages.MsgResult;
import uga.corse.themis.comm.messages.MsgRuntime;

import java.io.IOException;
import java.util.Collection;

public interface Dispatcher {
    interface Callback<T> {
         void onResponse(T response);
     }

     boolean broadcast(final Collection<String> addr, final String exclude, final MsgRuntime msg) throws IOException;
     boolean cmdSync(final String addr, final MsgRuntime msg) throws IOException;
     void cmdNoReply(final String addr, final MsgRuntime msg) throws IOException;
     void cmdAsync(final String addr, final MsgRuntime msg, final Callback<MsgResult> callback);
     void cmdAsyncNoReply(final String addr, final MsgRuntime msg);
     MsgResult cmdSyncObj(final String addr, final MsgRuntime msg) throws IOException;
}
