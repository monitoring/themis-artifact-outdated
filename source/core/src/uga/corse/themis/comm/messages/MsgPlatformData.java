package uga.corse.themis.comm.messages;


import uga.corse.themis.Platform;

public class MsgPlatformData implements MsgRuntime {

    private static final long serialVersionUID = 2679549814730294944L;
    private Platform platform;

    public MsgPlatformData(Platform platform) {
        this.platform = platform;
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    @Override
    public String toString() {
        return "(C: " + platform.getCountComponents() + ", M: " + platform.getCountMonitors() + ")";
    }
}
