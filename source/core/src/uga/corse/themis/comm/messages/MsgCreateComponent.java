package uga.corse.themis.comm.messages;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class MsgCreateComponent implements MsgRuntime {


    private static final long serialVersionUID = 5249597480211448115L;

    public static class InputInfo implements Serializable {


        private static final long serialVersionUID = 7694020315368908941L;
        String cls = null;
        String res = null;

        public InputInfo(String cls, String res) {
            this.cls = cls;
            this.res = res;
        }

        public String getCls() {
            return cls;
        }

        public String getRes() {
            return res;
        }


    }

    private String id;
    private String location;

    public Class getCls() {
        return cls;
    }

    public void setCls(Class cls) {
        this.cls = cls;
    }

    private Class cls;
    private List<InputInfo> input = new LinkedList<>();

    public MsgCreateComponent(String id, String location, Class cls) {
        this.id = id;
        this.location = location;
        this.cls = cls;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void addInput(String cls, String res) {
        synchronized(input) { input.add(new InputInfo(cls, res)); }
    }
    public List<InputInfo> getInput() {
        return input;
    }

    @Override
    public String toString() {
        return id;
    }
}
