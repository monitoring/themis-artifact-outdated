package uga.corse.themis.comm.messages;

import uga.corse.themis.comm.protocol.Message;

public class MsgMonitorPayload implements MsgRuntime {


    private static final long serialVersionUID = -8818345657157955783L;
    private Integer from;
    private Integer to;
    private Message message;

    public MsgMonitorPayload(Integer from, Integer to, Message message) {
        this.from       = from;
        this.to         = to;
        this.message    = message;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return to + " <- " + message.getClass().getSimpleName();
    }
}
