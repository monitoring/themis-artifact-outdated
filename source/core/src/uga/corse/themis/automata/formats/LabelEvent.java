package uga.corse.themis.automata.formats;

import uga.corse.themis.automata.Event;
import uga.corse.themis.automata.Label;

/**
 * Transition Label: Event
 * To check if a transition is possible, the events must match
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class LabelEvent implements Label {

    private Event event;

    public LabelEvent(Event event) {
        this.event = event;
    }

    @Override
    public boolean matches(Event e) {
        return event.equals(e);
    }

    @Override
    public String toString() {
        return event.toString();
    }

    public Event getEvent() {
        return event;
    }


}
