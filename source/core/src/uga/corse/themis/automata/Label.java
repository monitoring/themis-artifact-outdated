package uga.corse.themis.automata;

import java.io.Serializable;

/**
 * Automata Label
 * Used to select transitions that match an event
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public interface Label extends Serializable {
    boolean matches(Event e);
}
