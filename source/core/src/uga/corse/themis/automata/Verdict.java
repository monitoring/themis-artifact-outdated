package uga.corse.themis.automata;

/**
 * Possible Verdict Domain
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public enum Verdict {
    TRUE, FALSE, NA;

    /**
     * Return verdicts associated with boolean
     *
     * @param b Boolean
     * @return TRUE if b is boolean true, otherwise false
     */
    public static Verdict fromBoolean(boolean b) {
        if (b) return TRUE;
        else return FALSE;
    }

    /**
     * Combine verdicts
     * This method is used to merge two verdicts that could be at the same time
     * Basically an NA and anything cannot be merged
     *
     * @param v1 First Verdict
     * @param v2 Second Verdict
     * @return Resulting verdict
     */
    public static Verdict merge(Verdict v1, Verdict v2) {
        if (v1 == TRUE || v2 == TRUE)
            return TRUE;
        else if (v1 == FALSE || v2 == FALSE)
            return FALSE;
        else
            return NA;
    }

    /**
     * Check whether or not the verdict is final
     * A subset of the verdicts is usually final, and allows monitoring to terminate
     *
     * @return true if a final verdict
     */
    public boolean isFinal() {
        return Verdict.isFinal(this);
    }

    /**
     * See: {@link #isFinal()}
     *
     * @param v Verdict to check
     * @return True if final
     */
    public static boolean isFinal(Verdict v) {
        return v != null && v != NA;
    }
}
