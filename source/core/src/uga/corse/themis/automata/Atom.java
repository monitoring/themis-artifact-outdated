package uga.corse.themis.automata;

import java.io.Serializable;

import uga.corse.themis.symbols.AtomEmpty;

/**
 * The Atom represents an atomic symbol
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public interface Atom extends Serializable, Comparable<Atom> {
    /**
     * @return Return a string version of the atom
     */
    String observe();

    /**
     * @return Return a grouping object, so that atoms can be grouped with other atoms of the same object
     */
    Object group();

    /**
     * Denotes an atom representing an empty symbol
     *
     * @return is the atom an empty symbol?
     */
    boolean isEmpty();

    /**
     * Compare the atom to another object (mostly another atom)
     *
     * @param atom Compare against atom
     * @return equals to atom
     */
    boolean equals(Object atom);

    /**
     * @return Returns the empty symbol
     */
    static Atom empty() {
        return new AtomEmpty();
    }
}
