/**
 * Deviations
 * <p>
 * name <email>
 */
package uga.corse.themis.measurements;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;

/**
 * Compute Load/Spread
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Deviations<T> {
    private int total;
    private int totalRound;
    private Map<T, Integer> all;
    private Map<T, Integer> temp;
    private final Set<T> factors;


    public Deviations(final Set<T> set) {
        factors = set;
        all = init(set);
        temp = init(set);
        total = 0;
        totalRound = 0;
    }

    public synchronized void reset() {
        totalRound  = 0;
        temp        = init(factors);
    }

    public synchronized void push(T key) {
        push(key, 1);
    }

    public synchronized void  push(T key, Integer value) {
        all.put(key, all.get(key) + value);
        temp.put(key, temp.get(key) + value);
        total++;
        totalRound++;
    }

    public synchronized Integer getTotal() {
        return total;
    }
    public synchronized Integer getTotalSinceReset() {
        return totalRound;
    }
    public synchronized Integer getCountGlobal(T factor) {
        return all.get(factor);
    }
    public synchronized Integer getCountSinceReset(T factor) {
        return temp.get(factor);
    }
    public synchronized double[] compute() {

        if (total == 0) return new double[]{0d, 0d};

        double sumRound     = 0d;
        double sumAll       = 0d;
        double ideal        = 1d / factors.size();

        for(T factor : factors) {
            Integer nround = temp.get(factor);
            Integer ntotal = all.get(factor);

            if(totalRound > 0)
                sumRound += Math.pow(((((double) nround)/totalRound) - ideal), 2);
            sumAll   += Math.pow(((((double) ntotal)/total     ) - ideal), 2);
        }

        return new double[]{sumRound, sumAll};
    }

    private  Map<T, Integer> init(Set<T> from) {
        Map<T, Integer> map = new HashMap<>(from.size());

        for (T key : from)
            map.put(key, 0);

        return map;
    }

}
