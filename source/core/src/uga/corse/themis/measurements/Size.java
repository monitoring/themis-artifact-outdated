package uga.corse.themis.measurements;

/**
 * Size of an element
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public interface Size {
    Long get(DataSize s);
}
