package uga.corse.themis.measurements;

public enum DataSize {
    S_TIME,
    S_BOOL,
    S_STATE,
    S_ID,
    S_CTRL,
    S_VER,
    S_CHAR,
    S_OPER
}
