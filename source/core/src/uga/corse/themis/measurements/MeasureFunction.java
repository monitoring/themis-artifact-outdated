package uga.corse.themis.measurements;

/**
 * A measure update function
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public interface MeasureFunction {
    /**
     * Update the existing value with the return value
     *
     * @param existing
     * @param args     Parameters for the update
     * @return
     */
    Object update(Object existing, Object... args);

    public static MeasureFunction Default = new MeasureFunction() {
        @Override
        public Object update(Object existing, Object... args) {
            return existing;
        }
    };
}
