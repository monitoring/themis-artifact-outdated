package uga.corse.themis.measurements.size;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.comm.packets.MemoryPacket;
import uga.corse.themis.comm.protocol.Message;
import uga.corse.themis.measurements.Size;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.measurements.Config;

/**
 * Size of a memory transfer
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class PacketMem implements MsgSize<MemoryPacket> {
    @Override
    public Long getSize(MemoryPacket msg, Size size) {
        Memory<Atom> mem = (Memory<Atom>) msg.mem;
        if(mem == null) return 0l;

        Long total = 0l;
        for (Atom at : mem.getData())
            total += Config.atomsLength(at, size);
        return total;
    }
}
