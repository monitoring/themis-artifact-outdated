package uga.corse.themis.measurements.basic;

import java.io.Serializable;
import java.util.*;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.measurements.*;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.monitoring.memory.MemoryRO;
import uga.corse.themis.runtime.Node;
import uga.corse.themis.utils.Simplifier;


/**
 * Average simplifications per timestamp measures
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public privileged aspect Simplifications extends Instrumentation {
    protected static Deviations<String> devs;
    protected static Deviations<Integer> monDev;
    protected static Collection<Monitor> mons;
    protected static Map<Integer, Integer> evals;
    protected static String dummy = "";


    public pointcut callSimplify(Monitor mon):
            execution(* Simplifier.simplify(*))
                    && Commons.inMonitor(mon);



    after(Integer t) : Commons.monitoringStep(t) {
        synchronized (dummy) {
            if (t == 0) return;
            recompute(t);
            reset();
        }
    }


    after(Monitor mon): callSimplify(mon)
    {
        synchronized (dummy) {
            evals.put(mon.getID(), evals.get(mon.getID()) + 1);
            devs.push(mon.getComponentKey());
            monDev.push(mon.getID());
        }
    }


    protected synchronized int[] compute() {
        Integer max = 0;
        Integer sum = 0;

        for (Integer x : evals.values()) {
            max = Math.max(max, x);
            sum += x;
        }

        return new int[]{max, sum};
    }

    protected synchronized void updateLoad() {
        double[] d  = devs.compute();   //Deviations on Components
        double[] d2 = monDev.compute(); //Deviations on Monitors (Convergence, Load)


        update("simp_conv_comp", d[0] );
        update("simp_conv_mon" , d2[0]);

        update("simp_load_comp", d[1] );
        update("simp_load_mon" , d2[1]);
    }

    @Override
    protected void setup(Node n, Map<String, Serializable> tags) {
        setDescription("Simplifications");
        addMeasure(new Measure("simp_total", "Total Simplficiations", 0, Measures.addInt));
        addMeasure(new Measure("simp_maxmon", "MaxSimp /Mon/Round", 0, Measures.maxInt));
        addMeasure(new Measure("simp_maxmon_crit", "Critical Path", 0, Measures.addInt));
        addMeasure(new Measure("simp_conv_comp", "Component Convergence", 0d, Measures.addDouble));
        addMeasure(new Measure("simp_conv_mon", "Monitor Convergence", 0d, Measures.addDouble));
        addMeasure(new Measure("simp_load_comp", "Component Load", 0d, Measures.replace));
        addMeasure(new Measure("simp_load_mon", "Monitor Load", 0d, Measures.replace));


        Set<String> comps = new HashSet<>();
        Set<Integer> myMons = new HashSet<>();
        comps.addAll(n.getManagedComps().keySet());
        myMons.addAll(n.getManagedMonitors().keySet());

        devs    = new Deviations<>(comps);
        monDev  = new Deviations<>(myMons);
        mons    = n.getManagedMonitors().values();
        reset();
    }


    protected synchronized void reset() {
        evals = new HashMap<>(mons.size());
        for (Monitor myMon : mons)
            evals.put(myMon.getID(), 0);
        devs.reset();
        monDev.reset();
    }
    protected synchronized void recompute(int t) {
        int[] v = compute();
        update("simp_maxmon", v[0]);
        update("simp_maxmon_crit", v[0]);
        update("simp_total", v[1]);
        updateLoad();
    }
}
