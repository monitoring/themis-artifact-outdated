package uga.corse.themis.encoders;


import uga.corse.themis.automata.Atom;

public interface Encoder {
    Encoder DEFAULT = new EncIdentity();

    Atom encode(Atom from);
}
