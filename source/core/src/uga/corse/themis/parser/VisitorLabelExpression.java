package uga.corse.themis.parser;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.automata.Event;
import uga.corse.themis.expressions.bool.Atomic;
import uga.corse.themis.expressions.bool.BoolExpression;
import uga.corse.themis.expressions.bool.EventExpr;
import uga.corse.themis.parser.backend.LabelsParser.AtomContext;
import uga.corse.themis.parser.backend.LabelsParser.EV_PARContext;
import uga.corse.themis.parser.backend.LabelsParser.E_ATOMContext;
import uga.corse.themis.parser.backend.LabelsParser.E_COMPOSITEContext;
import uga.corse.themis.parser.backend.LabelsParser.E_PARENContext;
import uga.corse.themis.parser.backend.LabelsParser.E_SINGLEContext;
import uga.corse.themis.parser.backend.LabelsParser.L_EMPTYContext;
import uga.corse.themis.parser.backend.LabelsParser.ObligationContext;
import uga.corse.themis.parser.backend.LabelsBaseVisitor;
import uga.corse.themis.symbols.AtomObligation;
import uga.corse.themis.symbols.AtomString;


/**
 * Generates a boolean expression from parsing a Label from LTL2MON dot
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class VisitorLabelExpression extends LabelsBaseVisitor<BoolExpression> {
    private Event ev;
    private int depth = 0;

    @Override
    public BoolExpression visitL_EMPTY(L_EMPTYContext ctx) {
        return new Atomic(Atom.empty());
    }

    @Override
    public BoolExpression visitEV_PAR(EV_PARContext ctx) {
        return visit(ctx.event());
    }

    @Override
    public BoolExpression visitE_SINGLE(E_SINGLEContext ctx) {
        return visit(ctx.expr());
    }

    @Override
    public BoolExpression visitE_COMPOSITE(E_COMPOSITEContext ctx) {
        if (depth == 0) ev = new Event();
        Atomic ex = (Atomic) visit(ctx.expr());
        depth++;
        BoolExpression e = visit(ctx.event());
        if (e != null)
            ev.addObservation(((Atomic) e).getAtom());
        depth--;
        ev.addObservation(ex.getAtom());
        if (depth == 0) {
            return new EventExpr(ev);
        } else
            return null;
    }

    @Override
    public BoolExpression visitE_PAREN(E_PARENContext ctx) {
        return visit(ctx.expr());
    }

    @Override
    public BoolExpression visitE_ATOM(E_ATOMContext ctx) {
        return visit(ctx.atom());
    }

    @Override
    public BoolExpression visitAtom(AtomContext ctx) {
        if (ctx.obligation() != null)
            return visit(ctx.obligation());
        else
            return new Atomic(new AtomString(ctx.STRING().getText()));
    }

    @Override
    public BoolExpression visitObligation(ObligationContext ctx) {
        return new Atomic(new AtomObligation(
                new AtomString(ctx.STRING().getText()),
                Integer.parseInt(ctx.INT().getText()))
        );
    }
}
