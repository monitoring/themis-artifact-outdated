package uga.corse.themis.parser;

import uga.corse.themis.expressions.ltl.LTLBinary;
import uga.corse.themis.expressions.ltl.LTLExpression;
import uga.corse.themis.expressions.ltl.LTLOperator;
import uga.corse.themis.expressions.ltl.LTLSymbol;
import uga.corse.themis.expressions.ltl.LTLUnary;
import uga.corse.themis.parser.backend.LTLBaseVisitor;
import uga.corse.themis.parser.backend.LTLParser.LTL_ANDContext;
import uga.corse.themis.parser.backend.LTLParser.LTL_EQUIVContext;
import uga.corse.themis.parser.backend.LTLParser.LTL_FINALLYContext;
import uga.corse.themis.parser.backend.LTLParser.LTL_GLOBALContext;
import uga.corse.themis.parser.backend.LTLParser.LTL_IMPLIESContext;
import uga.corse.themis.parser.backend.LTLParser.LTL_NEGContext;
import uga.corse.themis.parser.backend.LTLParser.LTL_NEXTContext;
import uga.corse.themis.parser.backend.LTLParser.LTL_ORContext;
import uga.corse.themis.parser.backend.LTLParser.LTL_PARENContext;
import uga.corse.themis.parser.backend.LTLParser.LTL_RELEASEContext;
import uga.corse.themis.parser.backend.LTLParser.LTL_SYMContext;
import uga.corse.themis.parser.backend.LTLParser.LTL_UNTILContext;

/**
 * Parses LTL into LTLExpression
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class VisitorLTLExpression extends LTLBaseVisitor<LTLExpression> {

    @Override
    public LTLExpression visitLTL_SYM(LTL_SYMContext ctx) {
        return new LTLSymbol(ctx.getText());
    }

    @Override
    public LTLExpression visitLTL_PAREN(LTL_PARENContext ctx) {
        return visit(ctx.ltlexpr());
    }

    @Override
    public LTLExpression visitLTL_NEG(LTL_NEGContext ctx) {
        return new LTLUnary(
                LTLOperator.LTL_NEG,
                visit(ctx.ltlexpr()));
    }

    @Override
    public LTLExpression visitLTL_AND(LTL_ANDContext ctx) {
        return new LTLBinary(LTLOperator.LTL_AND,
                visit(ctx.ltlexpr(0)),
                visit(ctx.ltlexpr(1)));
    }

    @Override
    public LTLExpression visitLTL_OR(LTL_ORContext ctx) {
        return new LTLBinary(LTLOperator.LTL_OR,
                visit(ctx.ltlexpr(0)),
                visit(ctx.ltlexpr(1)));
    }

    @Override
    public LTLExpression visitLTL_IMPLIES(LTL_IMPLIESContext ctx) {
        return new LTLBinary(LTLOperator.LTL_IMPLIES,
                visit(ctx.ltlexpr(0)),
                visit(ctx.ltlexpr(1)));
    }

    @Override
    public LTLExpression visitLTL_EQUIV(LTL_EQUIVContext ctx) {
        return new LTLBinary(LTLOperator.LTL_EQUIV,
                visit(ctx.ltlexpr(0)),
                visit(ctx.ltlexpr(1)));
    }

    @Override
    public LTLExpression visitLTL_GLOBAL(LTL_GLOBALContext ctx) {
        return new LTLUnary(
                LTLOperator.LTL_GLOBALLY,
                visit(ctx.ltlexpr()));
    }

    @Override
    public LTLExpression visitLTL_FINALLY(LTL_FINALLYContext ctx) {
        return new LTLUnary(
                LTLOperator.LTL_FINALLY,
                visit(ctx.ltlexpr()));
    }

    @Override
    public LTLExpression visitLTL_NEXT(LTL_NEXTContext ctx) {
        return new LTLUnary(
                LTLOperator.LTL_NEXT,
                visit(ctx.ltlexpr()));
    }

    @Override
    public LTLExpression visitLTL_UNTIL(LTL_UNTILContext ctx) {
        return new LTLBinary(LTLOperator.LTL_UNTIL,
                visit(ctx.ltlexpr(0)),
                visit(ctx.ltlexpr(1)));
    }

    @Override
    public LTLExpression visitLTL_RELEASE(LTL_RELEASEContext ctx) {
        return new LTLBinary(LTLOperator.LTL_RELEASE,
                visit(ctx.ltlexpr(0)),
                visit(ctx.ltlexpr(1)));
    }
}
