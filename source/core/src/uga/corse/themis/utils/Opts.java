package uga.corse.themis.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Command-Line options interface
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Opts {
    /**
     * Return a global variable
     * Priority: System.properties overrides Environment
     *
     * @param key Variable name
     * @param def Default value if not set or empty
     * @return
     */
    public static String getGlobal(String key, String def) {
        if (System.getProperties().containsKey(key))
            return System.getProperty(key, def);

        String value = System.getenv(key);
        if (value == null || value.isEmpty())
            return def;
        else
            return value;
    }

    /**
     * Basic Exception to specify a wrong option
     *
     * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
     */
    @SuppressWarnings("serial")
    public static class OptInvalid extends Exception {
        String name;
        String error;

        public OptInvalid(String option, String error) {
            this.name = option;
            this.error = error;
        }

        public String getName() {
            return name;
        }

        public String getError() {
            return error;
        }

        @Override
        public String toString() {
            return name + ": " + error;
        }
    }

    /**
     * Option was  not provided
     *
     * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
     */
    @SuppressWarnings("serial")
    public static class OptMissing extends OptInvalid {
        public OptMissing(String name, String error) {
            super(name, error);
        }
    }

    /**
     * Option processing error
     *
     * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
     */
    @SuppressWarnings("serial")
    public static class OptError extends Exception {
        public OptError(String error) {
            super(error);
        }
    }

    /**
     * Option details
     *
     * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
     */
    public class OptDetails {
        /**
         * Option is optional
         */
        public boolean optional;
        /**
         * Number of args for function
         */
        public int args;
        /**
         * Description of function for usage
         */
        public String description;

        public OptDetails(boolean optional, int args, String description) {
            this.optional = optional;
            this.args = args;
            this.description = description;
        }

        public OptAction action;
    }

    Map<String, OptDetails> opts = new HashMap<String, OptDetails>();

    /**
     * Add an option
     *
     * @param name
     * @param optional
     * @param args
     * @param description
     */
    public void addOpt(String name, boolean optional, int args, String description) {
        this.opts.put(name, new OptDetails(optional, args, description));
    }

    /**
     * Bulk add options
     *
     * @param opts
     */
    public void addOpts(Object... opts) {
        if (opts.length % 4 != 0) return;
        for (int i = 0; i < opts.length - 1; i += 4) {
            addOpt((String) opts[i], (boolean) opts[i + 1],
                    (int) opts[i + 2], (String) opts[i + 3]);
        }
    }

    /**
     * Register an processing action for an option
     *
     * @param name
     * @param action
     * @return
     */
    public boolean registerAction(String name, OptAction action) {
        if (!opts.containsKey(name)) return false;
        opts.get(name).action = action;
        return true;
    }

    /**
     * Bulk Registration
     * [opt1, act1, opt2, act2, ...]
     *
     * @param acts
     */
    public void registerActions(Object... acts) {
        if (acts.length % 2 == 1) return;
        for (int i = 0; i < acts.length - 1; i += 2) {
            registerAction(acts[i].toString(), (OptAction) acts[i + 1]);
        }
    }

    /**
     * Execute the option actions and parse options
     * Returns a subarray of args which consists of things that were not processed
     *
     * @param args
     * @return
     * @throws OptInvalid
     * @throws OptMissing
     */
    public String[] process(String[] args) throws OptInvalid, OptMissing {
        HashSet<String> required = new HashSet<String>();
        LinkedList<String> actual = new LinkedList<String>();

        //Setup required params
        for (Entry<String, OptDetails> entry : opts.entrySet()) {
            if (!entry.getValue().optional)
                required.add(entry.getKey());
        }
        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            //Not an option
            if (!opts.containsKey(arg)) {
                actual.add(arg);
                continue;
            }
            OptDetails act = opts.get(arg);
            int next = act.args;
            String[] data = new String[next];
            //Option has args
            if (next > 0) {
                if (i + next >= args.length)
                    throw new OptInvalid(arg, "not enough arguments");


                for (int j = 1; j <= data.length; j++) {
                    if (opts.containsKey(args[i + j]))
                        throw new OptInvalid(arg, "Wrong value: " + args[i + j]);
                    data[j - 1] = args[i + j];
                }
            }
            try {
                act.action.process(data);
            } catch (OptError e) {
                throw new OptInvalid(arg, e.getMessage());
            } catch (Exception e) {
                throw new OptInvalid(arg, "Exception: " + e.toString());
            }

            required.remove(arg);

            i = i + next;
        }
        if (!required.isEmpty())
            throw new OptMissing("Not specified: ", required.toString());
        return actual.toArray(new String[actual.size()]);
    }

    public String printUsage() {
        StringBuilder sb = new StringBuilder();
        for (Entry<String, OptDetails> entry : opts.entrySet()) {
            sb.append(entry.getKey()).append(" : ");
            sb.append(entry.getValue().description);
            if (entry.getValue().optional)
                sb.append(" [Optional]");
            sb.append("\n");
        }
        return sb.toString();
    }

    public static String[] process(String[] args, Opts options) {
        try {
            args = options.process(args);
        } catch (OptInvalid e) {
            System.err.println(e.toString() + "\n Usage: ");
            System.err.println(options.printUsage());
            System.exit(1);
        }
        return args;
    }
}
