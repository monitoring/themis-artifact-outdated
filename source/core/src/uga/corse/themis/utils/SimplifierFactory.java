package uga.corse.themis.utils;

public class SimplifierFactory {
    private static Simplifier DEFAULT = null;

    /**
     * Default Simplifier
     *
     * @return
     */
    public static Simplifier getDefault() {
        if(DEFAULT == null)
           DEFAULT =  new SimplifierLTLFILT();
        return DEFAULT;
    }

    /**
     * Default Simplifier (spawn process)
     *
     * @return
     */
    public static Simplifier spawnDefault() {
        return new SimplifierLTLFILT();
    }
}
