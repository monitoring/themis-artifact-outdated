package uga.corse.themis.utils;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.Channels;
import java.nio.channels.CompletionHandler;
import java.util.Timer;
import java.util.TimerTask;

public class Server {
    private final static int DEFAULT_TIMEOUT = 15000;

    public interface ClientHandler {

        void handleClient(AsynchronousSocketChannel client,
                          ObjectInputStream in, ObjectOutputStream out);
    }

    private Integer timer = DEFAULT_TIMEOUT;
    final private Integer port;
    final private String host;

    private AsynchronousServerSocketChannel runningServer = null;

    public Server(String host, Integer port) {
        this.host = host;
        this.port = port;
    }

    public boolean isRunning() {
        return runningServer.isOpen();
    }

    public void setTimeout(int timer) {
        if(timer > 0) {
            this.timer = timer;
        }
    }
    public void start(final ClientHandler handleClient) throws IOException {

        runningServer = AsynchronousServerSocketChannel.open()
                .bind(new InetSocketAddress(host, port));

        runningServer.accept(null, new CompletionHandler<AsynchronousSocketChannel, Void>() {
            public void completed(final AsynchronousSocketChannel client, Void att) {

                runningServer.accept(null, this);

                InputStream in	 = Channels.newInputStream(client);
                OutputStream out = Channels.newOutputStream(client);
                Timer disconnect = new Timer();
                try {

                    ObjectInputStream input = new ObjectInputStream(in);
                    ObjectOutputStream output = new ObjectOutputStream(out);




                    if (client.isOpen()) {
                        TimerTask task = new TimerTask() {
                            @Override
                            public void run() {
                                try {
                                    client.close();
                                } catch (IOException e) {
                                }
                            }

                        };
                        disconnect.schedule(task, timer);
                        try {
                            handleClient.handleClient(client, input, output);
                        } finally {
                            task.cancel();
                        }
                    }
                } catch(IOException e) {
                    System.err.println(e);
                } finally {
                    disconnect.cancel();
                }
            }

            public void failed(Throwable exc, Void att) {
                System.err.println(exc);
            }
        });

    }

    public void stop() throws IOException {
        if (runningServer.isOpen())
            runningServer.close();
    }
}