package uga.corse.themis.utils;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeoutException;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.automata.Event;
import uga.corse.themis.automata.Transition;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.automata.formats.LabelEvent;
import uga.corse.themis.automata.formats.LabelExpression;
import uga.corse.themis.expressions.Expression;
import uga.corse.themis.expressions.ExpressionBinary;
import uga.corse.themis.expressions.ExpressionSymbol;
import uga.corse.themis.expressions.ExpressionUnary;
import uga.corse.themis.expressions.bool.*;
import uga.corse.themis.expressions.ltl.LTLExpression;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.monitoring.ehe.Representation;
import uga.corse.themis.monitoring.ehe.Rule;
import uga.corse.themis.parser.FrontExpressionParsers;
import uga.corse.themis.symbols.AtomEmpty;
import uga.corse.themis.symbols.AtomNegation;
import uga.corse.themis.symbols.AtomObligation;
import uga.corse.themis.symbols.AtomString;

/**
 * Utility Functions for expressions
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Expressions {

    public static StringBuilder formatShort(BoolExpression exp) {
        StringBuilder sb = new StringBuilder();
        if(exp instanceof BoolAnd) {
            sb.append("(");
            sb.append(formatShort(((BoolAnd) exp).getLeft()));
            sb.append("&");
            sb.append(formatShort(((BoolAnd) exp).getRight()));
            sb.append(")");
        }
        else if(exp instanceof BoolOr) {
            sb.append("(");
            sb.append(formatShort(((BoolOr) exp).getLeft()));
            sb.append("|");
            sb.append(formatShort(((BoolOr) exp).getRight()));
            sb.append(")");
        }
        else if(exp instanceof BoolNot) {
            sb.append("!");
            sb.append(formatShort(((BoolNot) exp).getOperand()));
        }
        else if(exp instanceof Atomic)
            sb.append(exp.toString());
        else if(exp instanceof True)
            sb.append("T");
        else if(exp instanceof False)
            sb.append("F");

        return sb;
    }

    /**
     * Parses a string into a boolean expression
     * Grammar defined by grammar/Labels.g4
     *
     * @param expr
     * @return
     */
    public static BoolExpression parseBoolean(String expr) {
        return FrontExpressionParsers.parseStringExpression(expr);
    }

    /**
     * Parses a string into a boolean expression
     * Grammar defined by grammar/Labels.g4
     *
     * @param expr
     * @return
     */
    public static Event parseEvent(String expr) {
        return FrontExpressionParsers.parseStringEvent(expr);
    }

    public static LTLExpression parseLTL(String ltl) {
        return FrontExpressionParsers.parseStringLTL(ltl);
    }

    /**
     * Generate obligations from a transition label that can be cast to an expression
     *
     * @param t
     * @param timestamp
     * @return
     */
    public static BoolExpression fromTransition(Transition t, int timestamp) {
        if (t.getLabel() instanceof LabelExpression) {
            BoolExpression e = (BoolExpression) t.getLabel();
            return build(e, timestamp);
        } else if (t.getLabel() instanceof LabelEvent) {
            LabelEvent ev = (LabelEvent) t.getLabel();
            return build(ev.getEvent(), timestamp);
        } else
            throw new Error("Transition Label Not Supported");
    }

    /**
     * Generate an expression from a transition label that can be cast to an expression
     *
     * @param t
     * @return
     */
    public static BoolExpression fromTransition(Transition t, boolean sort) {
        if (t.getLabel() instanceof LabelExpression) {
            BoolExpression e = (BoolExpression) t.getLabel();
            return e;
        } else if (t.getLabel() instanceof LabelEvent) {
            LabelEvent ev = (LabelEvent) t.getLabel();
            return build(ev.getEvent(), sort);
        } else
            throw new Error("Transition Label Not Supported");
    }

    /**
     * Convert an event to a boolean expression
     * Add obligations to timestamp
     *
     * @param e
     * @param timestamp
     * @return
     */
    public static BoolExpression build(Event e, int timestamp) {
        return build(e, timestamp, true);
    }
    /**
     * Convert an event to a boolean expression
     * Add obligations to timestamp
     *
     * @param e
     * @param timestamp
     * @param sorted Sort observations
     * @return
     */
    public static BoolExpression build(Event e, int timestamp, boolean sorted) {

        Set<Atom> syms = e.getAtoms();
        Iterator<Atom> iter;

        if(sorted) {
            SortedSet<Atom> set = new TreeSet<>(syms);
            iter = set.iterator();
        }
        else
            iter = syms.iterator();

        if (!iter.hasNext()) return new True();

        BoolExpression expr = build(iter.next(), timestamp);

        while (iter.hasNext())
            expr = new BoolAnd(expr, build(iter.next(), timestamp));

        return expr;
    }

    public static BoolExpression build(Event e, boolean sorted) {

        Set<Atom> syms = e.getAtoms();
        Iterator<Atom> iter;

        if(sorted) {
            SortedSet<Atom> set = new TreeSet<>(syms);
            iter = set.iterator();
        }
        else
            iter = syms.iterator();

        if (!iter.hasNext()) return new True();

        BoolExpression expr = build(iter.next());

        while (iter.hasNext())
            expr = new BoolAnd(expr, build(iter.next()));

        return expr;
    }

    public static BoolExpression build(Atom a) {
        if (a instanceof AtomNegation)
            return new BoolNot(build(((AtomNegation) a).getNegated()));
        else
            return new Atomic(a);
    }

    /**
     * Add obligation to an existing atom
     *
     * @param a
     * @param timestamp
     * @return
     */
    public static BoolExpression build(Atom a, int timestamp) {
        if (a instanceof AtomNegation)
            return new BoolNot(build(((AtomNegation) a).getNegated(), timestamp));
        else
            return new Atomic(new AtomObligation(a, timestamp));
    }

    /**
     * Generate an obligation from an expression e on a given timestamp
     *
     * @param e
     * @param timestamp
     * @return
     */
    public static BoolExpression build(BoolExpression e, int timestamp) {
        if (e instanceof LabelExpression)
            return build((BoolExpression) e.getOperands().get(0), timestamp);
        else if (e instanceof Atomic) {
            Atom a = (Atom) e.getOperands().get(0);
            Atom n = new AtomObligation(a, timestamp);
            return new Atomic(n);
        } else if (e instanceof True || e instanceof False) {
            return e;
        } else if (e instanceof BoolAnd) {
            return new BoolAnd(
                    build((BoolExpression) e.getOperands().get(0), timestamp),
                    build((BoolExpression) e.getOperands().get(1), timestamp)
            );
        } else if (e instanceof BoolOr) {
            return new BoolOr(
                    build((BoolExpression) e.getOperands().get(0), timestamp),
                    build((BoolExpression) e.getOperands().get(1), timestamp)
            );
        } else if (e instanceof BoolNot) {
            return new BoolNot(
                    build((BoolExpression) e.getOperands().get(0), timestamp)
            );
        } else if (e instanceof EventExpr) {
            Event newev = new Event();
            Event ev = ((EventExpr) e).getEvent();
            for (Atom a : ev.getAtoms()) {
                if (a instanceof AtomString)
                    newev.addObservation(new AtomObligation((AtomString) a, timestamp));
                else
                    newev.addObservation(a);
            }
            return new EventExpr(newev);
        } else
            return null;
    }

    public static BoolExpression reformatBoolean(BoolExpression expr, Map<String, BoolSym> symbols) {
        if (expr instanceof BoolBinary) {
            BoolExpression left = ((BoolBinary) expr).getLeft();
            BoolExpression right = ((BoolBinary) expr).getRight();
            left = reformatBoolean(left, symbols);
            right = reformatBoolean(right, symbols);

            if (expr instanceof BoolAnd) return new BoolAnd(left, right);
            if (expr instanceof BoolOr) return new BoolOr(left, right);
            else throw new Error("Could not Reformat: " + expr);
        }
        if (expr instanceof BoolNot)
            return new BoolNot(reformatBoolean(((BoolNot) expr).getOperand(), symbols));
        if (expr instanceof BoolSym) {
            if (expr instanceof Atomic) {
                Atom atom = ((Atomic) expr).getAtom();
                if (atom instanceof AtomObligation) {
                    String sym = ((AtomObligation) atom).getAtom().toString();
                    int t = ((AtomObligation) atom).getTimestamp();

                    String key = "o_" + t + "_" + sym;
                    symbols.put(key, (BoolSym) expr);
                    return new Atomic(new AtomString(key));
                } else if (atom instanceof AtomEmpty) {
                    symbols.put(atom.observe(), (BoolSym) expr);
                    return expr;
                } else {
                    return expr;
                }
            } else
                return expr;
        } else
            throw new Error("Could not Reformat: " + expr);
    }

    public static BoolExpression simplify(BoolExpression e) throws IOException, TimeoutException {
        return SimplifierFactory.getDefault().simplify(e);
    }

    public static class ProcessResult {
        public boolean changed = false;
        public boolean containsLeaf = false;
    }
    /**
     * Simplify and replace an expression
     *
     * @param e
     * @param mem
     * @return
     */
    public static BoolExpression process(BoolExpression e, Memory<?> mem, ProcessResult res) {
        if (e instanceof Atomic || e instanceof EventExpr) {
            Verdict v = e.eval(mem);

            if (v == Verdict.TRUE) {
                res.changed = true;
                return new True();
            }
            else if (v == Verdict.FALSE) {
                res.changed = true;
                return new False();
            }
            else
                return e;
        } else if (e instanceof True) {
            res.containsLeaf = true;
            return e;
        } else if (e instanceof False) {
            res.containsLeaf = true;
            return e;
        } else if (e instanceof BoolAnd) {
            BoolExpression left = process((BoolExpression) e.getOperands().get(0), mem, res);
            if (left instanceof False) return left;
            BoolExpression right = process((BoolExpression) e.getOperands().get(1), mem, res);
            if (right instanceof False) return right;

            if (left instanceof True) { res.containsLeaf = true; return right; }
            if (right instanceof True) { res.containsLeaf = true; return left; }

            return res.changed ? new BoolAnd(left, right) : e;
        } else if (e instanceof BoolOr) {
            BoolExpression left = process((BoolExpression) e.getOperands().get(0), mem, res);
            if (left instanceof True) return left;
            BoolExpression right = process((BoolExpression) e.getOperands().get(1), mem, res);
            if (right instanceof True) return right;

            if (left instanceof False) { res.containsLeaf = true; return right; }
            if (right instanceof False) { res.containsLeaf = true; return left; }

            return res.changed ? new BoolOr(left, right) : e;
        } else if (e instanceof BoolNot) {
            BoolExpression inline = process((BoolExpression) e.getOperands().get(0), mem, res);

            if (inline instanceof True) {
                res.containsLeaf = true;
                return new False();
            }
            else if (inline instanceof False) {
                res.containsLeaf = true;
                return new True();
            }
            else
                return res.changed ? new BoolNot(inline) : e;
        } else
            return null;
    }


    /**
     * Find the earliest obligation in a Representation
     * The stop specifies whether or not to stop after finding one in a rule
     * (Prevents to lookup earliest obligations in the next set of rules)
     *
     * @param rep
     * @param stop
     * @return
     */
    public static AtomObligation findEarliestObligation(Representation rep, boolean stop) {
        AtomObligation min = null;
        for (Rule r : rep.getRules().values()) {
            AtomObligation res = findEarliestObligation(r, stop);
            if (res != null) {
                min = (min != null) && min.compareTo(res) <= 0 ? min : res;
                if (stop) break;
            }
        }
        return min;
    }

    /**
     * Find earliest obligation in a Rule
     * The stop specifies whether or not to stop after finding it for one state in the rule
     *
     * @param r
     * @param stop
     * @return
     */
    public static AtomObligation findEarliestObligation(Rule r, boolean stop) {
        AtomObligation min = null;
        for (BoolExpression e : r.getPairs().values()) {
            AtomObligation res = findEarliestObligation(e);
            if (res != null) {
                min = (min != null) && min.compareTo(res) <= 0 ? min : res;
                if (stop) break;
            }
        }
        return min;
    }

    /**
     * Returns the smallest (earliest) obligation in an expression
     * Looks for AtomObligation and uses AtomObligation.compareTo
     *
     * @param e
     * @return
     */
    public static AtomObligation findEarliestObligation(BoolExpression e) {
        if (e instanceof Atomic) {
            Atom a = (Atom) e.getOperands().get(0);

            if (a instanceof AtomObligation) {
                if (a.isEmpty()) return null;
                return (AtomObligation) a;
            } else
                return null;

        } else if (e instanceof True || e instanceof False) {
            return null;
        } else if (e instanceof EventExpr) {
            Event v = ((EventExpr) e).getEvent();
            Set<Atom> atoms = v.getAtoms();
            Iterator<Atom> iter = atoms.iterator();
            AtomObligation min = null;
            while (iter.hasNext()) {
                Atom next = iter.next();
                if (next instanceof AtomObligation)
                    min = (min != null) && (min.compareTo(next) <= 0) ? min : (AtomObligation) next;

            }
            return min;
        } else if (e instanceof BoolAnd || e instanceof BoolOr) {
            AtomObligation left = findEarliestObligation((BoolExpression) e.getOperands().get(0));
            AtomObligation right = findEarliestObligation((BoolExpression) e.getOperands().get(1));
            if (left == null) return right;
            if (right == null) return left;

            return (left.compareTo(right) <= 0) ? left : right;
        } else if (e instanceof BoolNot) {
            return findEarliestObligation((BoolExpression) e.getOperands().get(0));
        } else
            return null;
    }

    /**
     * depth 		: depth of the expression
     * expressions 	: number of operators
     * leaves		: list of all leaf nodes
     */
    public static class ExprSize {
        public int depth = 0;
        public int expressions = 0;
        public List<ExpressionSymbol> leaves = new LinkedList<ExpressionSymbol>();

        public void updateDepth(int d) {
            depth = Math.max(depth, d);
        }
    }

    /**
     * Returns information about an expression
     *
     * @param expr
     * @return
     */
    public static ExprSize getLength(Expression expr) {
        ExprSize size = new ExprSize();
        updateLength(size, expr, 0);
        return size;
    }

    @SuppressWarnings("rawtypes")
    private static void updateLength(ExprSize size, Expression expr, int depth) {
        if (expr instanceof ExpressionBinary) {
            size.updateDepth(depth);
            updateLength(size, ((ExpressionBinary) expr).getLeft(), depth + 1);
            updateLength(size, ((ExpressionBinary) expr).getRight(), depth + 1);

        } else if (expr instanceof ExpressionUnary) {
            size.updateDepth(depth);
            updateLength(size, ((ExpressionUnary) expr).getOperand(), depth + 1);
        } else if (expr instanceof ExpressionSymbol) {
            size.leaves.add((ExpressionSymbol) expr);
        } else
            throw new Error("Expression not known: " + expr + " " + expr.getClass().getSimpleName());
    }
}
