package uga.corse.themis.utils.mapper;

import uga.corse.themis.automata.Atom;

/**
 * The interface to associate symbols
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public interface SymbolMapper {
    /**
     * Maps an atom to a component name
     *
     * @param ap Atom to check
     * @return Component name that handles the atom
     */
    String componentFor(Atom ap);

    /**
     * Maps a component to a monitor ID
     * Assumes 1:1 mapping between monitors and components
     * @param component Component name
     * @return Monitor ID of monitor for the component
     */
    Integer monitorFor(String component);

    /**
     * Maps an atom to a monitor ID
     *
     * @param ap Atom
     * @return ID
     */
    Integer monitorFor(Atom ap);
}
