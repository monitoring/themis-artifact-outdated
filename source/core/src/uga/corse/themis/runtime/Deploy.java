package uga.corse.themis.runtime;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import uga.corse.themis.Topology;
import uga.corse.themis.algorithms.orchestration.Orchestration;
import uga.corse.themis.comm.dispatcher.Dispatcher;
import uga.corse.themis.comm.dispatcher.Sockets;
import uga.corse.themis.comm.messages.*;

import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.Platform;
import uga.corse.themis.monitoring.components.AsyncObservations;
import uga.corse.themis.monitoring.specifications.Specification;
import uga.corse.themis.utils.Server;
import uga.corse.themis.utils.SpecLoader;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.channels.AsynchronousCloseException;
import java.util.*;
import java.util.concurrent.Semaphore;

public class Deploy {
    private static Logger log = LogManager.getLogger(Deploy.class);
    private interface Trigger {
        void trigger();
    }
    private static class StopCounter {
        Integer c = 0;
        Integer at = 0;
        Trigger trig = null;
        StopCounter(Integer stopAt, Trigger trig) { at = stopAt; this.trig = trig; }
        synchronized void increment()  {
            c++;
            if(c >= at) trig.trigger();
        }

    }

    private static final Server s  = new Server("localhost", 8090);
    /**
     * Basic deployment using orchestration
     * @param args
     * @throws Exception
     */
    public static void main(String[] args)  {
        Dispatcher disp   = new Sockets();
        Platform platform = new Platform();
        Topology top      = new Topology();
        String pfile      = args[0];
        Set<String> nodes = new HashSet<>();

        final Semaphore lock = new Semaphore(0);

        log.info("----- Booting: " + args[0]);

        try {
            log.info("[1/4] Deploying Components");
            deployComponents(disp, platform, pfile, top, nodes);

            log.info("=> Nodes Detected: " + nodes);
            log.info("[2/4] Deploying Monitors");

            Map<String, Specification> spec = SpecLoader.loadSpec(new File("spec.xml"));

            Orchestration orch = new Orchestration();
            orch.setSpecification(spec);
            Set<? extends Monitor> mons = orch.getMonitors(top);

            for(Monitor mon : mons)
                if (!deploy(disp, platform, mon))
                    fail(disp, nodes);


            log.info("[3/4] Sending Platform Information");
            for(String node : nodes)
                disp.cmdSync(node, new MsgPlatformData(platform));

            log.info("[4/5] Creating Notification Listener");


            final StopCounter stopper = new StopCounter(nodes.size(), ()->lock.release());

            s.start((client, in, out) -> {
                MsgRuntime msg = null;
                try {
                    msg = (MsgRuntime) in.readObject();

                    out.flush();
                    out.close();

                } catch (AsynchronousCloseException close) {

                    stopper.increment();

                } catch (Exception | Error e) {
                    log.warn(e.getMessage(), e);

                    try {
                        out.flush();
                        out.close();
                    } catch(Exception e2) {

                    }
                }
                if (msg == null)
                    throw new IllegalArgumentException("Wrong Message");

                if (msg instanceof MsgAbort) {
                    log.trace("Received Abort");
                    stopper.increment();
                }



            });

            //Start in 5 seconds
            log.info("[5/5] Sending Start Signal");
            if(!signalStart(disp, nodes, 5000l, "localhost:8090"))
                fail(disp, nodes);


        }
        catch(Exception e) {
            try {
                fail(disp, nodes);
                s.stop();
            } catch(Exception e2) {

            }
        }
        log.info("----- Waiting on Termination");
        try {
            lock.acquire();
        }catch(Exception e){

        }

        log.info("----- Completed");
    }

    public static void deployComponents(Dispatcher disp, Platform platform, String file, Topology top, Set<String> nodes)
            throws DocumentException, IOException
    {
        SAXReader reader = new SAXReader();
        Document document = reader.read(file);
        Element root = document.getRootElement();
        Iterator<Element> comps
                = root.elementIterator("component");


        while(comps.hasNext()) {
            Element c = comps.next();

            MsgCreateComponent info = new MsgCreateComponent(
                    c.attributeValue("id"),
                    c.attributeValue("location"),
                    AsyncObservations.class
            );
            top.add(info.getId());
            Iterator<Element> inputs = c.elementIterator("input");
            while (inputs.hasNext()) {
                Element in = inputs.next();
                info.addInput(in.attributeValue("class"), in.attributeValue("init"));

            }
            if(!deploy(disp, platform, info)) fail(disp, nodes);
            nodes.add(info.getLocation());
        }

    }

    public static boolean signalStart(Dispatcher disp, Set<String> nodes, Long delay, String notify)
            throws IOException
    {
        return signalStart(disp, nodes, delay, notify, new HashMap<>());
    }
    public static boolean signalStart(Dispatcher disp, Set<String> nodes, Long delay, String notify, Map<String, Serializable> tags)
    throws IOException
    {
        Date date = new Date((new Date()).getTime() + delay);
        //log.info("Start Signal: " + date);
        for(String node : nodes) {
            MsgStart start = new MsgStart(date);
            start.setNotify(notify);
            start.setTags(tags);
            if(!disp.cmdSync(node, start))
                return false;
        }
        return true;
    }
    public static boolean fail(Dispatcher disp, Set<String> nodes) throws IOException {
        try {
            throw new Exception();
        }
        catch(Exception e) {
            log.fatal("Failed: Notifying Nodes", e);
        }
        for(String addr : nodes)
            disp.cmdSync(addr, new MsgAbort());
        System.exit(1);
        return false;
    }
    public static boolean deploy(Dispatcher disp, Platform platform, MsgCreateComponent info) throws IOException {
        boolean success = disp.cmdSync(info.getLocation(), info);
        if(success)
            platform.setAddressComponent(info.getId(), info.getLocation());
        //log.info(info.getId() + "@" + info.getLocation() + " - " + success);
        return success;
    }
    public static boolean deploy(Dispatcher disp, Platform platform, Monitor m) throws IOException {
        String comp = m.getComponentKey();
        String addr = platform.getAddressComponent(comp);
        boolean res = disp.cmdSync(addr, new MsgAttachMonitor(comp, m));
        if(res)
            platform.setAddressMonitor(m.getID(), addr);
        //log.info(m.getID() + " <-> " + comp + "@" + addr + " - " + res);
        return res;
    }
}
