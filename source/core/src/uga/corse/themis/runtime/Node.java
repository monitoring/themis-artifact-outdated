package uga.corse.themis.runtime;



import uga.corse.themis.Platform;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.comm.dispatcher.Dispatcher;
import uga.corse.themis.comm.protocol.Network;
import uga.corse.themis.monitoring.Component;
import uga.corse.themis.monitoring.Monitor;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

public interface Node  {

    void        setAddress(String addr);
    String      getAddress();
    void        addComponent(Component comp);
    boolean     attachMonitor(String comp, Monitor mon);
    Platform    getPlatform();
    boolean     changePlatform(Platform platform);
    void        notifyVerdict(int monid, Verdict verdict);
    void        notifyAbort(int monid);
    void        setDispatcher(Dispatcher disp);
    Dispatcher  getDispatcher();


    int observerRegister(NodeObserver obs);
    void observerRemove(int id);

    void start(Map<String, Serializable> tags);
    void stop();
    void reset();
    boolean isStarted();


    Integer getMonitorCount();
    Integer getComponentCount();

    Component getComponentForMonitor(Integer integer);
    Set<Monitor> getMonitorsForComponent(String componentKey);

    Network getNetwork();
    void setNetwork(Network net);

    Map<String, Component> getManagedComps();
    Map<Integer, Monitor> getManagedMonitors();
}
