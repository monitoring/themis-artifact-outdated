package uga.corse.themis.algorithms.choreography;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import uga.corse.themis.expressions.ltl.LTLExpression;

import java.util.Set;

/**
 * MonitorNetwork for choreography network representation
 * This will be used to split a formula into a network
 * Represents a graph of (2^NetNode, 2^NetEdge)
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class MonitorNetwork implements Serializable {

    private static final long serialVersionUID = 1913436601252655064L;

    public class NetNode implements Comparable<NetNode> {
        public int id;
        public LTLExpression spec;
        public String comp;

        public NetNode(int id, LTLExpression spec, String comp) {
            this.id = id;
            this.spec = spec;
            this.comp = comp;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("<").append(id)
                    .append(", ").append(comp)
                    .append(", ").append(spec).append(">");
            return sb.toString();
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof NetNode)) return false;
            return obj.hashCode() == hashCode();
        }

        @Override
        public int hashCode() {
            return id;
        }

        @Override
        public int compareTo(NetNode o) {
            return this.id - o.id;
        }
    }

    public class NetEdge {
        public int from;
        public int to;

        public NetEdge(int from, int to) {
            this.from = from;
            this.to = to;
        }

        @Override
        public String toString() {
            return from + " -> " + to;
        }
    }

    public NetNode root;
    public HashMap<Integer, NetNode> nodes = new HashMap<>();
    public HashMap<Integer, Set<Integer>> edges = new HashMap<>();

    public void setRoot(NetNode root) {
        this.root = root;
    }

    public void addEdge(int from, int to) {
        if (!edges.containsKey(from))
            edges.put(from, new HashSet<>());
        edges.get(from).add(to);
    }

    public void addNode(int id, LTLExpression spec, String comp, boolean setRoot) {
        NetNode node = new NetNode(id, spec, comp);
        nodes.put(id, node);
        if (setRoot) root = node;
    }

    public void mergeGraphs(MonitorNetwork n1, MonitorNetwork n2) {
        for (NetNode node : n1.nodes.values())
            nodes.putIfAbsent(node.id, node);

        for (NetNode node : n2.nodes.values())
            nodes.putIfAbsent(node.id, node);

        addEdges(n1);
        addEdges(n2);
    }

    private void addEdges(MonitorNetwork n) {
        for (Entry<Integer, Set<Integer>> edge : n.edges.entrySet()) {
            Set<Integer> data = new HashSet<>();
            if (edges.containsKey(edge.getKey()))
                data = edges.get(edge.getKey());
            data.addAll(edge.getValue());
            edges.put(edge.getKey(), data);
        }
    }
}
