package uga.corse.themis.algorithms.choreography;

import uga.corse.themis.comm.protocol.Message;

/**
 * Message sent to kill a monitor
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class MessageKill implements Message {

    private static final long serialVersionUID = -1575179079663134275L;
    private int from;

    public MessageKill(int from) {
        this.from = from;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }


}
