package uga.corse.themis.algorithms.choreography;

import uga.corse.themis.automata.Verdict;
import uga.corse.themis.automata.VerdictTimed;
import uga.corse.themis.comm.protocol.Message;

/**
 * Message to announce verdict for a given timestamp
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class MessageVerdict implements Message {
    private static final long serialVersionUID = -3924439531297983715L;
    private VerdictTimed verdict;
    private int from;

    public MessageVerdict(Verdict v, int time, int from) {
        setVerdict(new VerdictTimed(v, time));
        setFrom(from);
    }

    public MessageVerdict(VerdictTimed v, int from) {
        setVerdict(v);
        setFrom(from);
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public Verdict getVerdict() {
        return verdict.getVerdict();
    }

    public void setVerdict(VerdictTimed verdict) {
        this.verdict = (verdict);
    }

    public void setVerdict(Verdict verdict) {
        this.verdict.setVerdict(verdict);
    }

    public int getTimestamp() {
        return verdict.getTimestamp();
    }

    @Override
    public String toString() {
        return "<" + from + ", " + verdict.toString() + ">";
    }
}
