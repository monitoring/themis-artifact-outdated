package uga.corse.themis.algorithms.migration;


import uga.corse.themis.automata.Automata;

/**
 * Migration Monitor with Round Robin swapping
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class MonMigrateRR extends MonMigrate {


    private static final long serialVersionUID = 4655033345303482664L;
    public MonMigrateRR(Integer id, Automata aut, Integer comps) {
        super(id, aut, comps);
    }

    @Override
    public int getNext() {
        return (getID() + 1) % comps;
    }
}
