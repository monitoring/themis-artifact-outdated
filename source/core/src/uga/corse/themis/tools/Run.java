package uga.corse.themis.tools;

import java.io.File;
import java.io.Serializable;
import java.nio.channels.AsynchronousCloseException;
import java.util.*;
import java.util.concurrent.Semaphore;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uga.corse.themis.Platform;
import uga.corse.themis.Topology;
import uga.corse.themis.bootstrap.Bootstrap;
import uga.corse.themis.bootstrap.StandardMonitoring;
import uga.corse.themis.comm.dispatcher.Dispatcher;
import uga.corse.themis.comm.dispatcher.Sockets;
import uga.corse.themis.comm.messages.MsgAbort;
import uga.corse.themis.comm.messages.MsgCreateComponent;
import uga.corse.themis.comm.messages.MsgPlatformData;
import uga.corse.themis.comm.messages.MsgRuntime;
import uga.corse.themis.monitoring.Component;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.monitoring.components.AsyncObservations;
import uga.corse.themis.monitoring.specifications.Specification;
import uga.corse.themis.periphery.TraceFile;
import uga.corse.themis.runtime.Deploy;
import uga.corse.themis.utils.OptAction;
import uga.corse.themis.utils.Opts;
import uga.corse.themis.utils.Server;
import uga.corse.themis.utils.SpecLoader;
import uga.corse.themis.utils.Opts.OptError;

/**
 * Command-Line tool to run the formulae
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Run {

    private static Logger log = LogManager.getLogger(Run.class);

    private static Opts options = initOptions();

    private static Opts initOptions() {
        Opts opts = new Opts();
        opts.addOpts(
                "-nc", false, 1, "Number of components",
                "-spec", false, 1, "Spec file",
                "-tid", false, 1, "Trace ID to read",
                "-tmax", true, 1, "Trace ID to read last, if specified, the traces from tid to tmax will be ran",
                "-nr", false, 1, "Length of the Run",
                "-in", false, 1, "Directory to read traces",
                "-alg", false, 1, "Full java classname of algorithm to use",
                "-node", true, 1, "Node address to execute run on",
                "-listen", true, 1, "Node address to ping back when done"
        );
        return opts;
    }

    public static class Options {
        public int ncomp;
        public int nrun;
        public int tid;
        public int tmax = -1;
        public String in;
        public Map<String, Specification> spec;
        public String filename = "";
        public StandardMonitoring alg;
        public boolean simplify = false;
        public String node = "localhost:8056";
        public String listen = "localhost:8090";
    }

    public static void npos(int n) throws OptError {
        if (n <= 0) throw new OptError(n + " must be > 0");
    }

    public static void nzer(int n) throws OptError {
        if (n < 0) throw new OptError(n + " must be >= 0");
    }
    public static void naddress(String s) throws OptError {
        if(s.isEmpty()) return;
        String[] sp = s.split(":");
        if(sp.length != 2) throw new OptError(sp + " is invalid");
        try {
            Integer.parseInt(sp[1]);
        } catch(Exception e) {
            throw new OptError(sp[1] + " is invalid");
        }
    }

    public static void main(String[] args) {
        Options config = new Options();
        options.registerActions(
                "-nc", (OptAction) x -> {
                    config.ncomp = Integer.parseInt(x[0]);
                    npos(config.ncomp);
                },
                "-nr", (OptAction) x -> {
                    config.nrun = Integer.parseInt(x[0]);
                    npos(config.nrun);
                },
                "-tid", (OptAction) x -> {
                    config.tid = Integer.parseInt(x[0]);
                    nzer(config.tid);
                },
                "-tmax", (OptAction) x -> {
                    config.tmax = Integer.parseInt(x[0]);
                    nzer(config.tmax);
                },
                "-simp", (OptAction) x -> config.simplify = true,
                "-spec", (OptAction) x -> {
                    File ltl = new File(x[0]);
                    if (!ltl.exists())
                        throw new OptError(x[0] + ": does not exist");
                    config.filename = ltl.getName();

                    config.spec = SpecLoader.loadSpec(ltl);
                },
                "-in", (OptAction) x -> {
                    /*config.in = new File(x[0]);
                    if (!config.in.exists())
                        throw new OptError(x[0] + ": directory does not exist");
                    if (!config.in.canRead())
                        throw new OptError(x[0] + ": not readable");
                        */
                    config.in = x[0];
                },
                "-alg", (OptAction) x -> {
                    @SuppressWarnings("unchecked")
                    Class<StandardMonitoring> cls =
                            (Class<StandardMonitoring>) Run.class.getClassLoader().loadClass(x[0]);
                    config.alg = cls.newInstance();
                },
                "-node", (OptAction) x -> {
                    naddress(x[0]);
                    config.node = x[0];
                },
                "-listen", (OptAction) x -> {
                    naddress(x[0]);
                    config.listen = x[0];
                }
        );

        args = Opts.process(args, options);

        if (config.spec == null) {
            System.err.println("No Spec given");
            options.printUsage();
        }

        Dispatcher disp   = new Sockets();
        Topology top      = new Topology();
        Set<String> nodes = new HashSet<>();

        String experimentAddress = config.node;

        nodes.add(experimentAddress);


        int trStop = Math.max(config.tid, config.tmax);



        Platform platform = new Platform();
        platform.setData(new Integer(config.nrun));

        List<MsgCreateComponent> comps = new LinkedList<>();



        for(int k = 0; k < config.ncomp; k++) {
            String name =  ((char) ('a' + k)) + "";
            top.add(name);
            MsgCreateComponent info = new MsgCreateComponent(
                    name, experimentAddress, AsyncObservations.class
            );
            comps.add(info);
        }

        config.alg.setSpecification(config.spec);
        Set<? extends Monitor> mons = config.alg.getMonitors(top);

        final Semaphore lock = new Semaphore(0);
        platform.getMonitors().clear();

        String[] sp = (config.listen.isEmpty()) ? null
                : config.listen.split(":");

        final Server s =
                (config.listen.isEmpty()) ? null :
                new Server(sp[0], Integer.parseInt(sp[1]));

        Map<String, Serializable> tags = new HashMap<>();
        tags.put("spec", config.filename);
        tags.put("alg", config.alg.getClass().getSimpleName());


        if(!config.listen.isEmpty()) {
            try {
                s.start((client, in, out) -> {
                    MsgRuntime msg = null;
                    try {
                        msg = (MsgRuntime) in.readObject();

                        out.flush();
                        out.close();

                    } catch (AsynchronousCloseException close) {
                        log.error("Crash possible!");
                        log.error(close.getMessage(), close);

                    } catch (Exception | Error e) {
                        log.error(e.getMessage(), e);

                        try {
                            out.flush();
                            out.close();
                        } catch (Exception e2) {

                        }
                    }
                    if (msg == null)
                        throw new IllegalArgumentException("Wrong Message");

                    if (msg instanceof MsgAbort) {
                        lock.release();
                    }
                });
            } catch (Exception e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }

        try {
            for (int i = config.tid; i <= trStop; i++) {
                tags.put("tid", i + "");
                Iterator<MsgCreateComponent> iter = comps.iterator();
                //Deploy Components
                for (int k = 0; k < config.ncomp; k++) {
                    String name = ((char) ('a' + k)) + "";
                    top.add(name);
                    MsgCreateComponent info = iter.next();
                    info.getInput().clear();
                    //File f = new File(config.in, i + "-" + name + ".trace");
                    //info.addInput("uga.corse.themis.periphery.TraceFile", f.getAbsolutePath());
                    info.addInput("uga.corse.themis.periphery.TraceFile",
                            config.in + (config.in.endsWith("/") ? "" : "/") + i + "-" + name + ".trace");

                    if (!Deploy.deploy(disp, platform, info))
                        Deploy.fail(disp, nodes);
                }


                //Deploy Monitors
                for (Monitor mon : mons)
                    if (!Deploy.deploy(disp, platform, mon))
                        Deploy.fail(disp, nodes);

                //Send Platform Data
                for (String node : nodes)
                    if (!disp.cmdSync(node, new MsgPlatformData(platform)))
                        Deploy.fail(disp, nodes);

                //Start and Wait
                if (!Deploy.signalStart(disp, nodes, 0l, config.listen.isEmpty() ? null : config.listen, tags))
                    Deploy.fail(disp, nodes);

                log.info("Monitoring Trace: " + i);

                if(!config.listen.isEmpty())
                    lock.acquire();


            }
        }
        catch(Exception e) {
            log.error(e.getLocalizedMessage(), e);
        }
    }
    //public static List<MsgCreateComponent> generateComponents(Options opts,)
}
