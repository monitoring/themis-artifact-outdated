/**
 * Control
 * <p>
 * name <email>
 */
package uga.corse.themis.periphery;

/**
 * Communication Signals with the periphery
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public interface Control {
}
