package uga.corse.themis.periphery;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.monitoring.memory.MemoryAtoms;
import uga.corse.themis.symbols.AtomString;

/**
 * Reads a trace from a file
 * Each event is on a line
 * <p> Example:</p>
 * <pre>
 * a0:t,b0:f,c0:t
 * a0:t,b0:t,c0:f
 * </pre>
 *
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class TraceFile implements Periphery {

    BufferedReader trace;
    FileReader fr;

    public TraceFile(File file) throws FileNotFoundException {
        fr = new FileReader(file);
        trace = new BufferedReader(fr);
    }
    public TraceFile(String fname) throws FileNotFoundException {
        fr = new FileReader(new File(fname));
        trace = new BufferedReader(fr);
    }

    @Override
    public Memory<Atom> next() throws IOException {
        Memory<Atom> mem = new MemoryAtoms();
        String line = null;

        line = trace.readLine();

        if (line == null || line.isEmpty()) return mem;

        String[] aps = line.split(",");
        for (String ap : aps) {
            String[] obs = ap.split(":");
            mem.add(new AtomString(obs[0]),
                    obs[1].equals("t"));
        }
        return mem;
    }

    @Override
    public void stop() {
        try {
            trace.close();
            fr.close();

        } catch (IOException e) {

        }
    }

    @Override
    protected void finalize() throws Throwable {
        trace.close();
        fr.close();
    }

    @Override
    public void notify(Control ctrl) throws IOException {
    }
}
