package uga.corse.themis.periphery;


import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public abstract class TraceSock implements Periphery {
    ServerSocket socket = null;
    Socket client = null;

    protected ServerSocket getServer() { return socket; }
    protected Socket       getClient() { return client; }

    protected abstract void setup(Socket client) throws IOException;

    public TraceSock(String res)
            throws IOException
    {
        String[] host = res.split(":");
        if(host.length != 2) throw new IllegalArgumentException(res);
        int port = Integer.parseInt(host[1]);

        socket = new ServerSocket(port, 1, InetAddress.getByName(host[0]));
        client = socket.accept();
        setup(client);
    }

    public TraceSock(String host, Integer port)
            throws IOException
    {
        socket = new ServerSocket(port, 1, InetAddress.getByName(host));
        client = socket.accept();
        setup(client);
    }

    @Override
    public void stop() {
        try {
            client.close();
            socket.close();
        }
        catch(IOException e) {

        }
    }
}
