#!/bin/php
<?php
$prefix = count($argv) > 0 ? $argv[1] : "";

while($f = fgets(STDIN)){
	$f = trim($f);
	$aps = explode(',', $f);
	$files = array();
	foreach($aps as $ap) {
		$fname = substr($ap, 0, 1);
		$files[$fname][] = $ap;
	}
	foreach($files as $fname => $fcont) {
		file_put_contents($prefix . $fname . ".trace", implode(',', $fcont) . "\n", FILE_APPEND);
	}
}

