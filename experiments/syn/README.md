---
title: Synthetic Benchmark
---

The synthetic benchmark consists of 4 experiments that vary number of components.
To maintain a reasonable size, all experiments are compressed (tar.gz).

## Exploring an Experiment

To explore the contents for an experiment simply unpack it.
For example for `comp3`

```
tar xvzf comp3.tgz
cd comp3
```

The pre-computed results are found in `localhost-8056.db` and `localhost-8057.db`, as the experiment was originally run on 2 nodes.

## Running an Experiment

Experiments are designed to be run on two nodes (running at localhost:8056 and localhost:8057).
To run an experiment extract the archive, delete or rename the result databases (localhost-8056.db and localhost-8057.db).

> *Note: if you are using the docker image, you can use `screen` or `tmux` to start multiple instances*

For example for `comp3`

```
tar xvzf comp3.tgz
cd comp3
rm *.db
```

To run the experiment you will need at least a terminal per node and one for the execution.
To do so you can use `screen` as follows, to spawn a screen use: 

```
screen
```

Then you can use the following to create and navigate (for tmux, use Ctrl+b instead):

* `Ctrl+a c` to create a new terminal in the same screen
* `Ctrl+a n` to navigate to the next terminal
* `Ctrl+a p` to navigate to the previous terminal


Start up two nodes

```
make node PORT=8056
make node PORT=8057
```

For each of the traces you will need to execute a run (check the available properties files for each trace type). Provide the properties file using `CFILE`

```
make run CFILE=setup-norm.properties
```

## Utilities

The Makefile provides basic targets for managing all experiments they are as follows:

* **unpack** : unpacks all the archives
* **pack**: compresses all folders prefixed with `comp`
* **keeparchives**: deletes all folders prefixed with `comp`
* **merge**: merges all the databases in all folders prefixed with `comp` in one `merge.db` database.

## Generating Other experiments

To generate more experiments you can view the documentation in the [experiment module](../../source/experiment).

## Short Experiment

A small experiment is provided in the [experiment module](../../source/experiment/example) for testing purposes.
