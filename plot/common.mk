out	  ?= data.dat
rplot ?= plot.r 
Q     ?= query.sql
SQLPROG?=sqlitebrowser

.PHONY: all gendata plot clean cleanall genraw explore

all: plot

prehook:

posthook:

${out}:
	sqlite3 -header -csv ${DB} "SELECT * FROM bench;" > ${out}

plot: prehook ${out}
	Rscript ${rplot}

explore:
	${SQLPROG} ${DB}

clean: posthook
	rm -f *.pdf *.tex ${out}
