# Common Plotting
source("../common.r")

# Set up graphs
cols<- "OrRd"
leg <- "top"

# Read data and process it by doing basic replacements and values ordering
dat<-read.csv("data.dat", header = TRUE)

dat$spec <- gsub("spec/", "", dat$spec) 
dat$spec <- gsub(".xml", "", dat$spec) 

# Basic Corrections for Metrics

# Adjust Min delay to 0 if there was no resolutions (metric will display max trace length)
dat$min_delay = ifelse(dat$resolutions == 0, 0, dat$min_delay)


# Adjust Length of Run, algorithms abort on a round + 1
dat$run_len   = ifelse(is.na(dat$verdict), dat$run_len, dat$run_len - 1)

# Generate Data Frame for metrics graphed

res<- dat %>% 
    group_by(alg, spec) %>%
    mutate(
      delay=zerodiv(sum_delay, resolutions)
    ) %>%
    summarise(
      delay_avg_1=mean(delay), delay_avg_2=sd(delay),
      msg_num_1=mean(zerodiv(msg_num,run_len)),  msg_num_2=sd(zerodiv(msg_num,run_len)),
      msg_data_1=mean(zerodiv(msg_data,run_len)),  msg_data_2=sd(zerodiv(msg_data,run_len)),
      simpm_1=mean(simp_maxmon), simpm_2=sd(simp_maxmon),
      simpc_1=mean(zerodiv(simp_maxmon_crit,run_len)), simpc_2=sd(zerodiv(simp_maxmon_crit,run_len)),
      conve_1=mean(zerodiv(exp_conv_comp,run_len)),  conve_2=sd(zerodiv(exp_conv_comp,run_len)),
      len=mean(run_len),
      verdictf=getverdict(verdict)
    ) %>%
    mutate(
      delay_avg=displaystat(delay_avg_1, delay_avg_2),
      msg_num=displaystat(msg_num_1, msg_num_2),
      msg_data=displaystat(msg_data_1, msg_data_2),
#      simpt=displaystat(simpt_1, simpt_2),
      simpm=displaystat(simpm_1, simpm_2),
      simpc=displaystat(simpc_1, simpc_2),
#    expt=displaystat(expt_1, expt_2),
      conve=displaystat(conve_1, conve_2)
    ) %>%
    select(alg, spec, 
           #verdictf, 
           #delay_max, 
           delay_avg, 
           #mdelay, 
           msg_num, msg_data, 
           #simpt, 
           simpm, 
           simpc,
           #expt, 
           #expm, 
           #convs, 
           conve
           #len
           )

message("Writing table to table.tex")
print(xtable(res), file="table.tex", include.rownames=FALSE)


# Data

tp <- dat %>%
    group_by(alg, spec) %>%
    summarize(
      msg_num  = mean(zerodiv(msg_num, run_len)),
      msg_data = mean(zerodiv(msg_data, run_len)),
      delay = mean(zerodiv(sum_delay, resolutions)),
      simpc = mean(zerodiv(simp_maxmon_crit, run_len)),
      simpm = mean(simp_maxmon),
      conv  = mean(zerodiv(exp_conv_comp, run_len)),
      expr  = mean(zerodiv(exp_maxmon_crit, run_len))
    )

# Msg Num

gdraw(ggplot(tp, aes(x=spec,  y= msg_num, fill=alg))+ 
        geom_bar(stat="identity", position="dodge") + 
        geom_text(aes(label=sprintf("%.2f", msg_num)), vjust=-1, color="black", position = position_dodge(0.9), size=2.3)+
        guides(fill=guide_legend(title="Algorithm")) +
        facet_grid(reformulate("factor(spec)"), scales="free", switch="both") 
) + 
  theme(strip.text.x = element_blank()) + 
  scale_fill_brewer(palette = cols) +
#  theme(legend.position = c(0.5, 0.95), legend.direction = "horizontal") + 
  labs(x="Specification", y="#Msgs (Normalized)") +
  ggsave("msgnum.pdf")


# Data Transfer

gdraw(ggplot(tp, aes(x=spec,  y= msg_data, fill=alg))+ 
  geom_bar(stat="identity", position="dodge") + 
  geom_text(aes(label=sprintf("%.0f", msg_data)), vjust=-1, color="black", position = position_dodge(0.9), size=2.3)+
  guides(fill=guide_legend(title="Algorithm")) +
  facet_grid(reformulate("factor(spec)"), scales="free", switch="both") 
) + 
  theme(strip.text.x = element_blank()) + 
  theme(legend.position=leg) + 
  scale_fill_brewer(palette = cols) +
  labs(x="Specification", y="Data (Normalized)") +
  ggsave("msgdata.pdf")


#gdraw(ggplot(tp %>% filter(spec %in% c("1", "3", "5", "15a")), aes(x=spec,  y= expr, fill=alg))+ 
#        geom_bar(stat="identity", position="dodge") + 
#        geom_text(aes(label=sprintf("%.2f", expr)), vjust=-1, color="black", position = position_dodge(0.9), size=2.3)+
#        guides(fill=guide_legend(title="Algorithm")) +
#        facet_grid(reformulate("factor(spec)"), scales="free", switch="both") 
#) + 
#  theme(strip.text.x = element_blank()) + 
#  theme(legend.position=leg) + 
#  scale_fill_brewer(palette = cols) +
#  labs(x="Specification", y="Data (Normalized)") +
#  ggsave("exprc.pdf")


# Delay

gdraw(ggplot(tp, aes(x=spec,  y= delay, fill=alg))+ 
        geom_bar(stat="identity", position="dodge") + 
        geom_text(aes(label=sprintf("%.2f", delay)), vjust=-1, color="black", position = position_dodge(0.9), size=2.3)+
        guides(fill=guide_legend(title="Algorithm")) +
        facet_grid(reformulate("factor(spec)"), scales="free", switch="both") 
) + 
  scale_y_continuous(trans="log1p") +
  theme(strip.text.x = element_blank()) + 
  scale_fill_brewer(palette = cols) +
  labs(x="Specification", y="Average Delay (log scale)") +
  ggsave("delay.pdf")


# Crit
gdraw(ggplot(tp %>% filter(alg != "Orch"), aes(x=spec,  y= simpc, fill=alg))+ 
        geom_bar(stat="identity", position="dodge") + 
        geom_text(aes(label=sprintf("%.2f", simpc)), vjust=-1, color="black", position = position_dodge(0.9), size=2.3)+
        guides(fill=guide_legend(title="Algorithm")) +
        facet_grid(reformulate("factor(spec)"), scales="free", switch="both") 
) + 
  theme(strip.text.x = element_blank()) + 
  theme(legend.position=leg) + 
  scale_fill_brewer(palette = cols) +
  labs(x="Specification", y="Critical Simplifications (Normalized)") +
  ggsave("simpc.pdf")

# Max
gdraw(ggplot(tp %>% filter(alg != "Orch"), aes(x=spec,  y= simpm, fill=alg))+ 
        geom_bar(stat="identity", position="dodge") + 
        geom_text(aes(label=sprintf("%.2f", simpm)), vjust=-1, color="black", position = position_dodge(0.9), size=2.3)+
        guides(fill=guide_legend(title="Algorithm")) +
        facet_grid(reformulate("factor(spec)"), scales="free", switch="both") 
) + 
  theme(strip.text.x = element_blank()) + 
  theme(legend.position=leg) + 
  scale_fill_brewer(palette = cols) +
  scale_y_log10() + 
  labs(x="Specification", y="Maximum Simplifications per Monitor (Worst-Case, log scale)") +
  ggsave("simpm.pdf")


# Conv
gdraw(ggplot(tp, aes(x=spec,  y= conv, fill=alg))+ 
        geom_bar(stat="identity", position="dodge") + 
        geom_text(aes(label=sprintf("%.2f", conv)), vjust=-1, color="black", position = position_dodge(0.9), size=2.3)+
        guides(fill=guide_legend(title="Algorithm")) +
        facet_grid(reformulate("factor(spec)"), scales="free", switch="both") 
) + 
  theme(strip.text.x = element_blank()) + 
  theme(legend.position=leg) + 
  scale_fill_brewer(palette = cols) +
  labs(x="Specification", y="Convergence") +
  ggsave("conv.pdf")

#themis_plot(dat, "zerodiv(sum_delay, resolutions)", "Average Delay (Normalized)",
#            myx="spec", labx = "Specification",
#            pal=cols, type=themis_box_nonotch, legend="top")   + 
#  coord_cartesian(ylim=c(0,5)) + 
#  theme(legend.text=element_text(size=20)) +
#  theme(legend.position = c(0.5, 0.95), legend.direction = "horizontal") + 
#  ggsave("delay.pdf")




