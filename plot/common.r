library(reshape2)
library(ggplot2)
library(dplyr)
library(xtable)

qt <- function(x, y) {
  return(quantile(x)[y]);
}

gdraw <- function(g) {
  return( g
    + theme_minimal()
    + theme(axis.line = element_line(colour = 'black', size = .5))
  )
}

themis_box <- function(myplot) {
  return (myplot + geom_boxplot(position=position_dodge(1), width=.3, outlier.shape=NA, notch = TRUE, notchwidth = 0.1))
}
themis_box_nonotch <- function(myplot) {
  return (myplot + geom_boxplot(position=position_dodge(1), width=.3, outlier.shape=NA, notch = FALSE))
}

# Plotting Function
themis_plot <- function(dat, myy, laby, type=themis_box, myx="factor(comps)", labx="Components", myf="alg", labf="Algorithm", legend="bottom", pal="Set1") {
  
return (gdraw(type(ggplot(data=dat,  aes_string(x=myx, y=myy, fill=myf)))
#+ geom_boxplot(aes_string(x=myx, y=myy, fill=myf), position=position_dodge(1), width=.3, outlier.shape=NA)
+ xlab(labx)
+ ylab(laby)
+ guides(fill=guide_legend(title=labf))
+	facet_grid(reformulate(myx), scales="free", switch="both") 

) + 
  theme(strip.text.x = element_blank()) + 
#  theme(axis.text.x=element_blank())  + 
  theme(legend.position=legend) + 
  scale_fill_brewer(palette = pal))
}

zerodiv <- function(delay, res) {
  return (ifelse(res==0, 0, delay/res))
}
displaystat <- function(v1, v2) {
  return(sprintf("%.2f (%.2f)", v1, v2))
}

# Convert verdict to replace in LaTeX
getverdict <- function(v) return (if(is.na(v[1])) "??" else if(v[1]) "TT" else "FF" )

